#ifndef INCLUDE_GLOBALS_H_
#define INCLUDE_GLOBALS_H_
#include <map>
#include <utility>
#include <vector>
#include <unordered_map>

extern bool FAIL_FAST;

extern const bool MALE;
extern const bool FEMALE;
extern const int NUM_SEX;
extern const int NUM_AGE_GROUPS;
extern const int MIN_AGE;
extern const int MAX_AGE;
extern const int MAX_DRINKS;
extern const int AGE_GROUPS[9];

extern const int MAX_DRINK_LEVEL;
extern const int MIN_DRINK_LEVEL;

extern std::map<int, std::pair<double, double> > MEAN_SD_LOOKUP_TABLE;
extern std::unordered_map<std::string, double> deathRateTable;//storing death rate by characteristics
extern std::unordered_map<std::string, double> migrationOutTable;//storing migration-out rate by characteristics


//TODO: Move theory-specific global variables out of core

/**** RATIONAL CHOICE ****/
extern double UNIT_PRICE[40];
extern double TEMP_LEGAL_RISK;
extern double DECAY_BASE_RATE;
extern int DAYS_TO_DEVELOP_WITHDRAWAL;
extern int WITHDRAWAL_WASHOUT_DIVISOR; 
extern double HOURS_FREE_TIME_MEAN; 
extern double HOURS_FREE_TIME_SD;
extern int TOTAL_YEARS_CONSUMPTION_STOCK; 
extern int HEAVY_DRINKS_PER_DAY;
/**** RATIONAL CHOICE ****/


/**** CONTAGION ****/
/*
extern const int POTENTIAL_BUDDIES_SIZE;

extern  int NUMBER_X_PROCESS;
extern  int NUMBER_Y_PROCESS;
extern  int ORIGIN_X;
extern  int ORIGIN_Y;
extern  int EXTENT_X;
extern  int EXTENT_Y;
extern  double MEAN_CONTINUOUS_REACH;

extern  double INFLUENCE_BETA;
extern  double SELECTION_BETA;
extern  double OUTDEGREE_BETA;
extern  double RECIPROCITY_BETA;
extern  double PREFERENTIAL_ATTACHMENT_BETA;
extern  double TRANSITIVE_TRIPLES_BETA;
extern  double AGE_SIMILARITY_BETA;
extern  double SEX_SIMILARITY_BETA;
extern  double INFLUENCE_LAMBDA;
extern  double SELECTON_LAMBDA;
*/
/**** CONTAGION ****/


/**** NORMS ****/
// Globals for the injunctive norms transformational mechanisms
extern int INJUNCTIVE_THRESHOLD; // Threshold above which society punishes drinking
extern double INJUNCTIVE_PROPORTION; // Proportion of people that have to be above the injunctive threshold to trigger
extern double INJUNCTIVE_ADJUSTMENT; // Factor by which the injunctive norm decreases when criteria are met for too heavy drinking
extern double INJ_RELAXATION_GAMMA_ADJUSTMENT; // the factor by which the injunctive norm gamma increases when relaxation criteria are met
extern double INJ_RELAXATION_LAMBDA_ADJUSTMENT; // the factor by which the injunctive norm lambda decreases when relaxation criteria are met
extern double INJ_PUNISHMENT_GAMMA_ADJUSTMENT; // the factor by which the injunctive norm gamma decreases when punishment criteria are met
extern double INJ_PUNISHMENT_LAMBDA_ADJUSTMENT; // the factor by which the injunctive norm lambda increases when punishment criteria are met

// Globals for descriptive norms
extern const int DESCRIPTIVE_INCUBATION_PERIOD; // number of days within which the descriptive norm has to be above the injunctive norm
extern const double DESCRIPTIVE_INCUBATION_PERCENT; // percentage of days over which the descriptive norm has to be above the injunctive norm

// This is the bias in perceiving quantity
extern double PERCEPTION_BIAS; // a weighting factor when perceiving descriptive norms

// Time periods over which drinking behaviour it is evaluated
extern int N_DAYS_DESCRIPTIVE; // is used to generate descriptive norms on prevalence and average quantity
extern int COMP_DAYS_PUNISH; // Used in the binge punishment (average drinks in comp days > injunctive threshold)
extern int COMP_DAYS_RELAX; // Used in the injunctive relaxation (prevalence of one drink over comp days)

extern const double DEFAULT_D_NORM; // This is what the descriptive norm for the gateway defaults to if weights = 0
extern const double DEFAULT_D_NORM_QUANT; // This is what the descriptive norm defaults to if weights = 0; // This is what the descriptive norm defaults to if weights = 0
extern const double DEFAULT_D_NORM_QUANT_SD;

extern const int MAX_DRINK_LEVEL;
extern const int MIN_DRINK_LEVEL;

// CP Edit: add the discounting factor to decrease payoff with increasing drinks
extern double DISCOUNT_MALE;
extern double DISCOUNT_FEMALE;

extern double DESIRE_MULTIPLIER_ABSTAINER;
extern double DESIRE_MULTIPLIER_DRINKER;
/**** NORMS ****/

/**** ROLES ****/
extern const int THRESHOLD_QUANTITY_HEAVEY_MALE_DRINKER;
extern const int THRESHOLD_QUANTITY_HEAVEY_FEMALE_DRINKER;
extern const int IDFOROUTPUT;
extern bool ROLES_SOCIALISATION_ON;
/**** ROLES ****/

#endif /* INCLUDE_GLOBALS_H_ */
