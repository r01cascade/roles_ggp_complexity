#include "MediatorForOneTheory.h"

#include "TheoryMediator.h"
#include "Theory.h"

MediatorForOneTheory::MediatorForOneTheory(std::vector<Theory*> theoryList) : TheoryMediator(theoryList) {
	mpTheory = theoryList[0];
}

void MediatorForOneTheory::mediateSituation() {
	mpTheory->doSituation();
}

void MediatorForOneTheory::mediateGatewayDisposition() {
	mpTheory->doGatewayDisposition();
}

void MediatorForOneTheory::mediateNextDrinksDisposition() {
	mpTheory->doNextDrinksDisposition();
}

void MediatorForOneTheory::mediateNonDrinkingActions(){
	mpTheory->doNonDrinkingActions();
}
