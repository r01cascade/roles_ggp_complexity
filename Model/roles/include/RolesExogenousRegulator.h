/* RolesExogenousRegulator.h */

#ifndef INCLUDE_ROLESEXOGENOUSREGULATOR_H_
#define INCLUDE_ROLESEXOGENOUSREGULATOR_H_

#include "repast_hpc/SharedContext.h"

#include "Regulator.h"
#include "Agent.h"

class RolesExogenousRegulator : public Regulator {

private:
	repast::SharedContext<Agent> *mpContext;
	double** adjustmentLevels;

public:
	RolesExogenousRegulator(repast::SharedContext<Agent> *context);
	//RolesExogenousRegulator(repast::SharedContext<Agent> *context, int noRow, int noCol);
	~RolesExogenousRegulator();

	void updateAdjustmentLevel() override {};
	//double getAdjustmentLevel(int sex, int ageGroup);
};

#endif /* INCLUDE_ROLEEXOGENOUSREGULATOR_H_ */
