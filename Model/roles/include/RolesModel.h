/* RolesModel.h */

#ifndef INCLUDE_ROLESMODEL_H_
#define INCLUDE_ROLESMODEL_H_

#include <boost/mpi.hpp>
#include "repast_hpc/Schedule.h"

#include "Model.h"

class RolesModel : public Model {
private:
	//std::vector<std::string> autonomyVector; //autonomy values read from file
	//std::vector<std::string> educationStatusVector; //education values read from file//not considering in Iteration 1
	//std::vector<std::string> levelofInvolvementEducationVector; //level of involvement on education values read from file//not considering in Iteration 1
	//std::vector<std::string> maritalStatusVector; //marital values read from file
	int mIndexMaritalStatus = -1;
	//std::vector<std::string> levelofInvolvementMarriageVector; //level of involvement on marriage values read from file
	int mIndexMaritalInvolvement = -1;
	//std::vector<std::string> parenthoodStatusVector; //parenthood values read from file
	int mIndexParenthoodStatus = -1;
	//std::vector<std::string> levelofInvolvementParenthoodVector; //level of involvement on parenthood values read from file
	int mIndexParenthoodInvolvement = -1;
	//std::vector<std::string> employmentStatusVector; //employment values read from file
	int mIndexEmploymentStatus = -1;
	//std::vector<std::string> levelofInvolvementEmploymentVector; //level of involvement on employment values read from file
	int mIndexEmploymentInvolvement = -1;
	//std::vector<std::string> abilityofCopingRoleStrainVector; //ability of coping role strain value read from file
	int mIndexAbilityCopingRoleStrain = -1;
	//std::vector<std::string> roleSkillVector;//ability of coping multi roles
	int mIndexRoleSkill = -1;
	int mIndexGatewayDispositionInitialization = -1;
	//std::vector<std::string> rolesDiscontinuityVector; //role discontinuity value read from file//not considering in Iteration 1
	//std::vector<std::string> roleStrainVector; //role strain vector value read from file
	//std::vector<std::string> expRoleOverloadVector;//exponent for calculating role overload

	//role load
	std::vector<std::string> roleLoadBeta1Vector;//iteration2
	std::vector<std::string> roleLoadBeta2Vector;//iteration2
	std::vector<std::string> roleLoadBeta3Vector;//iteration2
	std::vector<std::string> roleLoadBeta4Vector;//iteration2

	//role drinking opportunity parameters
	std::vector<std::string> roleDrinkingOpportunityBeta1Vector;//iteration2
	std::vector<std::string> roleDrinkingOpportunityBeta2Vector;//iteration2
	std::vector<std::string> roleDrinkingOpportunityBeta3Vector;//iteration2
	std::vector<std::string> roleDrinkingOpportunityBeta3bVector;//iteration2
	std::vector<std::string> roleDrinkingOpportunityBeta4Vector;//iteration2

	std::vector<std::string> roleStrainAffectingGatewayDispositionBetaVector;//iteration2
	std::vector<std::string> roleStrainAffectingNextDrinkDispositionBetaVector;//iteration2

	//rate of entry to roles according to age groups and sex
	std::vector<std::string> rateofEntryMarriageVector;//base rate of entry marriage
	std::vector<std::string> rateofEntryParenthoodVector; //base rate of entry parenthood
	std::vector<std::string> rateofEntryEmploymentVector; //base rate of entry employment
	//rate of exit from roles according to age groups and sex
	std::vector<std::string> rateofExitMarriageVector; //base rate of exit marriage
	std::vector<std::string> rateofExitParenthoodVector; //base rate of exit parenthood
	std::vector<std::string> rateofExitEmploymentVector; //base rate of exit employment

	//role expectancies according to age groups and sex
	std::vector<std::string> roleExpectancyMarriageVector; //role expectancies for marriage
	std::vector<std::string> roleExpectancyParenthoodVector;  //role expectancies for parenthood
	std::vector<std::string> roleExpectancyEmploymentVector;  //role expectancies for employment



	//role disposition parameters
	//std::vector<std::string> rolesDispositionBaselineMeanVector;
	int mIndexDispositionBaselineMean = -1;
	//std::vector<std::string> rolesDispositionBaselineSDVector;
	int mIndexDispositionBaselineSD = -1;
	std::vector<std::string> roleSocialisationBetaMoreRolesVector;
	std::vector<std::string> roleSocialisationBetaLessRolesVector;
	//std::vector<std::string> rolesDispositionBetaAffectedByRoleStrainMaleVector;
	//std::vector<std::string> rolesDispositionBetaAffectedByRoleStrainFemaleVector;
	double mSocialisationSpeed;

	//role opportunity parameters
	std::vector<std::string> baselineOpportunityVector;
	double mBalanceOpportunity;

public:
	RolesModel(std::string propsFile, int argc, char** argv, boost::mpi::communicator* comm);
	~RolesModel();

	void initMediatorAndTheoryWithRandomParameters(Agent *agent) override;
	void initMediatorAndTheoryFromFile(Agent *agent, std::vector<std::string> info) override;
	void initForTheory(repast::ScheduleRunner& runner) override;
	void readRankFileForTheory(std::string rankFileName) override;

	void initStatisticCollector() override;
	void addTheoryAnnualData(repast::SVDataSetBuilder& builder) override;
};


#endif /* INCLUDE_ROLESMODEL_H_ */
