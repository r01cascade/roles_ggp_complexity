#ifndef INCLUDE_ROLES_STATCOLLECTOR_H_
#define INCLUDE_ROLES_STATCOLLECTOR_H_

#include "StatisticsCollector.h"

class RolesStatisticsCollector : public StatisticsCollector {

public:
	RolesStatisticsCollector(repast::SharedContext<Agent>* pPopulation);
	void collectTheoryAgentStatistics() override;
};

#endif /* INCLUDE_ROLES_STATCOLLECTOR_H_ */