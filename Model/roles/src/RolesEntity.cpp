/* RolesEntity.h */

#include <vector>
#include <stdio.h>
#include "globals.h"
#include "repast_hpc/Utilities.h"
#include "repast_hpc/RepastProcess.h"

#include "RolesEntity.h"
#include "Agent.h"
#include "RolesExogenousRegulator.h"

RolesEntity::RolesEntity(std::vector<Regulator*> regulatorList, \
						std::vector<double> powerList, \
						int transformationalInterval, \
						std::vector<std::string> roleExpectancyMarriage, \
						std::vector<std::string> roleExpectancyParenthood, \
						std::vector<std::string> roleExpectancyEmployment, \
						repast::SharedContext<Agent> *context
						) :	StructuralEntity(regulatorList, powerList, transformationalInterval) {

	mpContext = context;

	mRoleExpectancyMarriage3DArray = createDouble3DArray(NUM_SEX,NUM_AGE_GROUPS,20,roleExpectancyMarriage);
	mRoleExpectancyParenthood3DArray = createDouble3DArray(NUM_SEX,NUM_AGE_GROUPS,20,roleExpectancyParenthood);
	mRoleExpectancyEmployment3DArray = createDouble3DArray(NUM_SEX,NUM_AGE_GROUPS,20,roleExpectancyEmployment);

	/* for (int page = 1; page < 2; page++)
		for (int row = 0; row < 1; row++)
			for (int column = 0; column < 9; column++)
				std::cout<< mRoleExpectancyMarriage3DArray[page][row][column] << " "; */
				//create a 2D arrays: NUM_SEX x NUM_AGE_GROUPS
	mRoleExpectancyMarriage2DArray = new double*[NUM_SEX];
	mRoleExpectancyParenthood2DArray = new double*[NUM_SEX];
	mRoleExpectancyEmployment2DArray = new double*[NUM_SEX];
	for(int i = 0; i < NUM_SEX; ++i) {
		mRoleExpectancyMarriage2DArray[i] = new double[NUM_AGE_GROUPS];
		mRoleExpectancyParenthood2DArray[i] = new double[NUM_AGE_GROUPS];
		mRoleExpectancyEmployment2DArray[i] = new double[NUM_AGE_GROUPS];
	}
}

RolesEntity::~RolesEntity() {

	deleteDouble3DArray(20,NUM_SEX,mRoleExpectancyMarriage3DArray);
	deleteDouble3DArray(20,NUM_SEX,mRoleExpectancyParenthood3DArray);
	deleteDouble3DArray(20,NUM_SEX,mRoleExpectancyEmployment3DArray);
}

void RolesEntity::doTransformation() {
	int currentTick = (int)floor(repast::RepastProcess::instance()->getScheduleRunner().currentTick());
	if (currentTick==0 || currentTick % mTransformationalInterval == 0) {
		updateRoleExpectancyArrays();
	}

	updateYear();
}

//provide current prevalance of age-sex expectancy for marital roles
double RolesEntity::getRoleExpectancyMarriage(int sex, int age_group){
	//return mRoleExpectancyMarriage3DArray[currentYear][sex][age_group];
	return mRoleExpectancyMarriage2DArray[sex][age_group];
}

double RolesEntity::getRoleExpectancyParenthood(int sex, int age_group){
	//return mRoleExpectancyParenthood3DArray[currentYear][sex][age_group];
	return mRoleExpectancyParenthood2DArray[sex][age_group];
}

double RolesEntity::getRoleExpectancyEmployment(int sex, int age_group){
	//return mRoleExpectancyEmployment3DArray[currentYear][sex][age_group];
	return mRoleExpectancyEmployment2DArray[sex][age_group];
}

int*** RolesEntity::createInt3DArray(int x, int y, int z, std::vector<std::string> aVector){
	//allocating memory to 3D array
	int*** a3DArray;
	int index = 0;

	if (x*y*z == aVector.size()){
		a3DArray = new int**[z];
		for (int i = 0; i < z; i++)
			a3DArray[i] = new int*[x];
		for (int i = 0; i < z; i++)
			for (int j = 0; j < x; j++)
				a3DArray[i][j] = new int[y];

		for (int i = 0; i < z; i++)
			for (int j = 0; j < x; j++)
				for (int k = 0; k < y; k++){
					a3DArray[i][j][k] = repast::strToInt(aVector[index]);
					index++;
				}
	}
	return a3DArray;
}

double*** RolesEntity::createDouble3DArray(int x, int y, int z, std::vector<std::string> aVector){
	//allocating memory to 3D array
	double*** a3DArray;
	int index = 0;

	if (x*y*z == aVector.size()){
		a3DArray = new double**[z];
		for (int i = 0; i < z; i++)
			a3DArray[i] = new double*[x];
		for (int i = 0; i < z; i++)
			for (int j = 0; j < x; j++)
				a3DArray[i][j] = new double[y];

		for (int i = 0; i < z; i++)
			for (int j = 0; j < x; j++)
				for (int k = 0; k < y; k++){
					a3DArray[i][j][k] = repast::strToDouble(aVector[index]);
					index++;
				}
	}
	return a3DArray;
}

void RolesEntity::updateYear(){
	#ifdef DEBUG
		std::cout<<"Current day = "<<currentDay<<std::endl;
		std::cout<<"Current year = "<<currentYear<<std::endl;
		std::cout<<"                         "<<std::endl;
	#endif

	if (++currentDay == 365 + 1){
		currentYear++;
		currentDay = 1;
		//std::cout<<"currentYear = "<< currentYear << std::endl;
	}
}

void*** RolesEntity::deleteInt3DArray(int x,int y,int ***Array){
	for (int i = 0; i < x; i++){
		for (int j = 0; j < y; j++){
			delete[] Array[i][j];
		}
		delete[] Array[i];
	}
	delete[] Array;
}

void*** RolesEntity::deleteDouble3DArray(int x,int y,double ***Array){
	for (int i = 0; i < x; i++){
		for (int j = 0; j < y; j++){
			delete[] Array[i][j];
		}
		delete[] Array[i];
	}
	delete[] Array;
}

void RolesEntity::updateRoleExpectancyArrays() {
	//create arrays to count agents in a group
	int** pCountInGroup = new int*[NUM_SEX];
	for(int i = 0; i < NUM_SEX; ++i) {
		pCountInGroup[i] = new int[NUM_AGE_GROUPS];
	}

	//reset count arrays to 0
	for(int i = 0; i < NUM_SEX; ++i) {
		for (int j=0; j<NUM_AGE_GROUPS; ++j) {
			pCountInGroup[i][j] = 0;
			mRoleExpectancyMarriage2DArray[i][j] = 0;
			mRoleExpectancyParenthood2DArray[i][j] = 0;
			mRoleExpectancyEmployment2DArray[i][j] = 0;
		}
	}

	//loop through agents to calculate total
	std::vector<Agent*> agents;
	mpContext->selectAgents(mpContext->size(), agents);
	std::vector<Agent*>::const_iterator iter = agents.begin();
	std::vector<Agent*>::const_iterator iterEnd = agents.end();
	while (iter != iterEnd) {
		int sex = (*iter)->getSex();
		int ageGroup = (*iter)->findAgeGroup();

		mRoleExpectancyMarriage2DArray[sex][ageGroup] += (*iter)->getMaritalStatus();
		mRoleExpectancyParenthood2DArray[sex][ageGroup] += (*iter)->getParenthoodStatus();
		mRoleExpectancyEmployment2DArray[sex][ageGroup] += (*iter)->getEmploymentStatus();
		pCountInGroup[sex][ageGroup]++;

		iter++;
	}

	//caculate aggregate info
	for (int i=0; i<NUM_SEX; ++i) {
		for (int j=0; j<NUM_AGE_GROUPS; ++j) {
			if (pCountInGroup[i][j] == 0) {
				mRoleExpectancyMarriage2DArray[i][j] = 0;
				mRoleExpectancyParenthood2DArray[i][j] = 0;
				mRoleExpectancyEmployment2DArray[i][j] = 0;
			} else {
				mRoleExpectancyMarriage2DArray[i][j] /= pCountInGroup[i][j];
				mRoleExpectancyParenthood2DArray[i][j] /= pCountInGroup[i][j];
				mRoleExpectancyEmployment2DArray[i][j] /= pCountInGroup[i][j];
			}
		}
	}

	//delete count array
	for(int i = 0; i < NUM_SEX; ++i) {
		delete[] pCountInGroup[i];
	}
	delete[] pCountInGroup;

/*
	//year 0 - DEBUG
	if (currentYear == 0) {
		for (int i=0; i<NUM_SEX; ++i) {
			for (int j=0; j<NUM_AGE_GROUPS; ++j) {
				printf("YEAR 0 == %d %d === %.2f \t %.2f \t %.2f \n", i, j,
									mRoleExpectancyMarriage2DArray[i][j] - mRoleExpectancyMarriage3DArray[currentYear][i][j],
									mRoleExpectancyParenthood2DArray[i][j] - mRoleExpectancyParenthood3DArray[currentYear][i][j],
									mRoleExpectancyEmployment2DArray[i][j] - mRoleExpectancyEmployment3DArray[currentYear][i][j]
								);
			}
		}
	}

	//DEBUG
	int sex = 1;
	int age_group = 3;
	printf("TEST === %.2f \t %.2f \t %.2f \n",
						mRoleExpectancyMarriage2DArray[sex][age_group] - mRoleExpectancyMarriage3DArray[currentYear][sex][age_group],
						mRoleExpectancyParenthood2DArray[sex][age_group] - mRoleExpectancyParenthood3DArray[currentYear][sex][age_group],
						mRoleExpectancyEmployment2DArray[sex][age_group] - mRoleExpectancyEmployment3DArray[currentYear][sex][age_group]
					);
*/
}
