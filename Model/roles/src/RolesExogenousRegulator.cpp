/* RolesExogenousRegulator.h */

#include <vector>
#include "globals.h"

#include "RolesExogenousRegulator.h"

/*The regulator is changed to be exogenous. So that it needs to consider how to perform.
  Below is the code from norms model, not suitable for roles model.*/

RolesExogenousRegulator::RolesExogenousRegulator(repast::SharedContext<Agent> *context){

}
/* RolesExognousRegulator::RolesExognousRegulator(repast::SharedContext<Agent> *context, int noRow, int noCol) {
	mpContext = context;

	//create a 2D array: noRow x noCol
	adjustmentLevels = new double*[noRow];
	for(int i = 0; i < noRow; ++i)
		adjustmentLevels[i] = new double[noCol];
} */

RolesExogenousRegulator::~RolesExogenousRegulator() {
	/* for(int i = 0; i < 2; ++i) //TODO: this number 2 is from noRow, should noRow be pulled from globals?
		delete[] adjustmentLevels[i];
	delete[] adjustmentLevels; */
}

/* void RolesExognousRegulator::updateAdjustmentLevel() {
	std::vector<Agent*> agents;
	mpContext->selectAgents(mpContext->size(), agents);

	for (int currentSex = 0; currentSex != 2; ++currentSex) {
		for (int currentAgeGroup = 0; currentAgeGroup != NUM_AGE_GROUPS;
				++currentAgeGroup) {
			int totalN = 0;
			int heavyDrinkerN = 0;
			double proportionHeavyDrinkers = 0.0;
			std::vector<Agent*>::const_iterator iter =
					agents.begin();
			std::vector<Agent*>::const_iterator iterEnd =
					agents.end();

			while (iter != iterEnd) {
				if ((*iter)->getSex() == currentSex
						&& (*iter)->findAgeGroup() == currentAgeGroup) {
					++totalN;
					if ((*iter)->getNumberDrinksToday() >= INJUNCTIVE_THRESHOLD) {
						++heavyDrinkerN;
					}
				}
				iter++;
			}

			if (totalN !=0){
				proportionHeavyDrinkers = double(heavyDrinkerN) / double(totalN);
			} else {
				//std::cout << "There are no people in this demographic here" << std::endl;
			} */

/* #ifdef DEBUG
			std::stringstream msg;
			msg << "I am currently in cell: " << currentSex << ","
					<< currentAgeGroup << "\n My injunctive norms are: "
					<< mpInjunctiveNorms[currentSex][currentAgeGroup] << "\n"
					<< " The number of people here is: "
					<< totalN << "\n"
					<< " The number of heavy drinkers here is: "
					<< heavyDrinkerN << "\n"
					<< " the proportion of heavy drinkers is: "
					<< proportionHeavyDrinkers << "\n";
			std::cout << msg.str();
#endif */

			/* if (proportionHeavyDrinkers > INJUNCTIVE_PROPORTION) {
				adjustmentLevels[currentSex][currentAgeGroup] = INJUNCTIVE_PUNISHMENT;
			} else {
				adjustmentLevels[currentSex][currentAgeGroup] = 1;
			} */

/* #ifdef DEBUG
			std::stringstream msg;
			msg
					<< "Heavy drinkers had to be punished. The new injunctive norm of this group is now: "
					<< mpInjunctiveNorms[currentSex][currentAgeGroup]
					<< "\n";
			std::cout << msg.str();
#endif */
/* 		}
	}
} */

/* double RolesExognousRegulator::getAdjustmentLevel(int sex, int ageGroup) {
	return adjustmentLevels[sex][ageGroup];
} */
