/* RolesModel.cpp */

#include "repast_hpc/SVDataSetBuilder.h"
#include "repast_hpc/AgentId.h"
#include "repast_hpc/RepastProcess.h"
#include "repast_hpc/Utilities.h"
#include <vector>

#include "RolesModel.h"

#include "Agent.h"
#include "RolesStatisticsCollector.h"
#include "TheoryMediator.h"
#include "Theory.h"
#include "StructuralEntity.h"
#include "MediatorForOneTheory.h"
#include "RolesTheory.h"
#include "RolesEntity.h"
#include "RolesExogenousRegulator.h"


RolesModel::RolesModel(std::string propsFile, int argc, char** argv, boost::mpi::communicator* comm) :
					Model(propsFile, argc, argv, comm) {

	//read initial information for role specific characters
	//repast::tokenize(props->getProperty("roles.educatoin.status"), educationStatusVector, ",");//not considering in Iteration 1
	//repast::tokenize(props->getProperty("roles.level.involvement.education"), levelofInvolvementEducationVector, ",");//not considering in Iteration 1
	//repast::tokenize(props->getProperty("roles.marital.status"), maritalStatusVector, ",");
	//repast::tokenize(props->getProperty("roles.level.involvement.marriage"), levelofInvolvementMarriageVector, ",");
	//repast::tokenize(props->getProperty("roles.parenthood.status"), parenthoodStatusVector, ",");
	//repast::tokenize(props->getProperty("roles.level.involvement.parenthood"), levelofInvolvementParenthoodVector, ",");
	//repast::tokenize(props->getProperty("roles.employment.status"), employmentStatusVector, ",");
	//repast::tokenize(props->getProperty("roles.level.involvement.employment"), levelofInvolvementEmploymentVector, ",");
	//repast::tokenize(props->getProperty("roles.ability.coping.role.strain"), abilityofCopingRoleStrainVector, ",");
	//repast::tokenize(props->getProperty("roles.skill"), roleSkillVector, ",");
	//repast::tokenize(props->getProperty("roles.discontinuity"), rolesDiscontinuityVector, ",");//not considering in Iteration 1

	//repast::tokenize(props->getProperty("roles.theory.exponent.role.overload"), expRoleOverloadVector, ",");

	//read role entity vectors from file
	//repast::tokenize(props->getProperty("roles.entity.rate.entry.marriage"), rateofEntryMarriageVector, ",");
	//repast::tokenize(props->getProperty("roles.entity.rate.entry.parenthood"), rateofEntryParenthoodVector, ",");
	//repast::tokenize(props->getProperty("roles.entity.rate.entry.employment"), rateofEntryEmploymentVector, ",");
	//repast::tokenize(props->getProperty("roles.entity.rate.exit.marriage"), rateofExitMarriageVector, ",");
	//repast::tokenize(props->getProperty("roles.entity.rate.exit.parenthood"), rateofExitParenthoodVector, ",");
	//repast::tokenize(props->getProperty("roles.entity.rate.exit.employment"), rateofExitEmploymentVector, ",");

	//repast::tokenize(props->getProperty("roles.entity.role.expectancy.marriage"), roleExpectancyMarriageVector, ",");
	//repast::tokenize(props->getProperty("roles.entity.role.expectancy.parenthood"), roleExpectancyParenthoodVector, ",");
	//repast::tokenize(props->getProperty("roles.entity.role.expectancy.employment"), roleExpectancyEmploymentVector, ",");

	//read role entity vectors from csv file
	std::string roleParamFilename = props->getProperty("role.parameter.file");

	ifstream roleParam(roleParamFilename);
	std::string row;
	while (!roleParam.eof()) {
        std::getline(roleParam, row);
        if (roleParam.bad() || roleParam.fail()) {
            break;
        }
        auto fields = readCSVRow(row);
		if (fields[0] == "roles.entity.rate.entry.marriage"){
			fields.erase(fields.begin());
			rateofEntryMarriageVector = fields;
		}
		if (fields[0] == "roles.entity.rate.entry.parenthood"){
			fields.erase(fields.begin());
			rateofEntryParenthoodVector = fields;
		}
		if (fields[0] == "roles.entity.rate.entry.employment"){
			fields.erase(fields.begin());
			rateofEntryEmploymentVector = fields;
		}
		if (fields[0] == "roles.entity.rate.exit.marriage"){
			fields.erase(fields.begin());
			rateofExitMarriageVector = fields;
		}
		if (fields[0] == "roles.entity.rate.exit.parenthood"){
			fields.erase(fields.begin());
			rateofExitParenthoodVector = fields;
		}
		if (fields[0] == "roles.entity.rate.exit.employment"){
			fields.erase(fields.begin());
			rateofExitEmploymentVector = fields;
		}
		if (fields[0] == "roles.entity.role.expectancy.marriage"){
			fields.erase(fields.begin());
			roleExpectancyMarriageVector = fields;
		}
		if (fields[0] == "roles.entity.role.expectancy.parenthood"){
			fields.erase(fields.begin());
			roleExpectancyParenthoodVector = fields;
		}
		if (fields[0] == "roles.entity.role.expectancy.employment"){
			fields.erase(fields.begin());
			roleExpectancyEmploymentVector = fields;
		}
	}

	//read in paramenters for situational mechanism (for calibration purpose)
	//repast::tokenize(props->getProperty("roles.theory.exponent.role.overload"), expRoleOverloadVector, ",");
	repast::tokenize(props->getProperty("role.load.beta1"), roleLoadBeta1Vector, ",");//iteration2
	repast::tokenize(props->getProperty("role.load.beta2"), roleLoadBeta2Vector, ",");//iteration2
	repast::tokenize(props->getProperty("role.load.beta3"), roleLoadBeta3Vector, ",");//iteration2
	repast::tokenize(props->getProperty("role.load.beta4"), roleLoadBeta4Vector, ",");//iteration2

	repast::tokenize(props->getProperty("role.drinking.opportunity.beta1"), roleDrinkingOpportunityBeta1Vector, ",");//iteration2
	repast::tokenize(props->getProperty("role.drinking.opportunity.beta2"), roleDrinkingOpportunityBeta2Vector, ",");//iteration2
	repast::tokenize(props->getProperty("role.drinking.opportunity.beta3"), roleDrinkingOpportunityBeta3Vector, ",");//iteration2
	repast::tokenize(props->getProperty("role.drinking.opportunity.beta3b"), roleDrinkingOpportunityBeta3bVector, ",");//iteration2
	repast::tokenize(props->getProperty("role.drinking.opportunity.beta4"), roleDrinkingOpportunityBeta4Vector, ",");//iteration2

	repast::tokenize(props->getProperty("role.strain.affecting.gateway.disposition.beta"), roleStrainAffectingGatewayDispositionBetaVector, ",");//iteration2
	repast::tokenize(props->getProperty("role.strain.affecting.next.drink.disposition.beta"), roleStrainAffectingNextDrinkDispositionBetaVector, ",");//iteration2

	//read in parameters for role disposition calculation
	//repast::tokenize(props->getProperty("roles.disposition.baseline.mean"), rolesDispositionBaselineMeanVector, ",");
	//repast::tokenize(props->getProperty("roles.disposition.baseline.sd"), rolesDispositionBaselineSDVector, ",");
	repast::tokenize(props->getProperty("role.socialisation.beta.more.roles"), roleSocialisationBetaMoreRolesVector, ",");
	repast::tokenize(props->getProperty("role.socialisation.beta.less.roles"), roleSocialisationBetaLessRolesVector, ",");
	mSocialisationSpeed = repast::strToDouble(props->getProperty("role.socialisation.speed"));
	//repast::tokenize(props->getProperty("roles.disposition.beta.disposition.affected.by.role.strain.male"), rolesDispositionBetaAffectedByRoleStrainMaleVector, ",");
	//repast::tokenize(props->getProperty("roles.disposition.beta.disposition.affected.by.role.strain.female"), rolesDispositionBetaAffectedByRoleStrainFemaleVector, ",");

	//read in parameters for role opportunity calculation
	repast::tokenize(props->getProperty("roles.opportunity.baseline.opportunity"), baselineOpportunityVector, ",");
	mBalanceOpportunity = repast::strToDouble(props->getProperty("roles.opportunity.balance.opportunity"));

	//socialisation on-off
	ROLES_SOCIALISATION_ON = repast::strToInt(props->getProperty("role.socialisation.on"));

	//read parameters from file and store in a table for init agents function (used by Model as well)
	int rank = repast::RepastProcess::instance()->rank();
	std::string fileNameRankProperty = "file.rank" + std::to_string(rank);
	std::string fileNameRank = props->getProperty(fileNameRankProperty);
	readRankFileForTheory(fileNameRank);

	//regulators that affect the structural entities
	std::vector<Regulator*> regulatorList;
	regulatorList.push_back( new RolesExogenousRegulator(&context) );

	//power of each regulator (sum = 1)
	std::vector<double> powerList;
	//powerList.push_back(1.0);
	int rolesEntityInterval = repast::strToInt(props->getProperty("transformational.interval.roles.entity"));

	//create a structural entity (for transformational mechanisms)
	StructuralEntity *roleEntity = new RolesEntity(regulatorList, \
													powerList, \
													rolesEntityInterval, \
													roleExpectancyMarriageVector, \
													roleExpectancyParenthoodVector, \
													roleExpectancyEmploymentVector, \
													&context
													);
	structuralEntityList.push_back(roleEntity);
}

RolesModel::~RolesModel() {}

void RolesModel::addTheoryAnnualData(repast::SVDataSetBuilder& builder) {
	// Role stats by role count (0 to 3)
	StatDataSource<int>* outPop_RoleCount0 = mpCollector->getDataSource<int>("OutPop_RoleCount0");
	builder.addDataSource(repast::createSVDataSource("OutPop_RoleCount0", outPop_RoleCount0, std::plus<int>()));

	StatDataSource<int>* outPop_RoleCount1 = mpCollector->getDataSource<int>("OutPop_RoleCount1");
	builder.addDataSource(repast::createSVDataSource("OutPop_RoleCount1", outPop_RoleCount1, std::plus<int>()));

	StatDataSource<int>* outPop_RoleCount2 = mpCollector->getDataSource<int>("OutPop_RoleCount2");
	builder.addDataSource(repast::createSVDataSource("OutPop_RoleCount2", outPop_RoleCount2, std::plus<int>()));

	StatDataSource<int>* outPop_RoleCount3 = mpCollector->getDataSource<int>("OutPop_RoleCount3");
	builder.addDataSource(repast::createSVDataSource("OutPop_RoleCount3", outPop_RoleCount3, std::plus<int>()));


	StatDataSource<int>* outDrinker_RoleCount0 = mpCollector->getDataSource<int>("OutDrinker_RoleCount0");
	builder.addDataSource(repast::createSVDataSource("OutDrinker_RoleCount0", outDrinker_RoleCount0, std::plus<int>()));

	StatDataSource<int>* outDrinker_RoleCount1 = mpCollector->getDataSource<int>("OutDrinker_RoleCount1");
	builder.addDataSource(repast::createSVDataSource("OutDrinker_RoleCount1", outDrinker_RoleCount1, std::plus<int>()));

	StatDataSource<int>* outDrinker_RoleCount2 = mpCollector->getDataSource<int>("OutDrinker_RoleCount2");
	builder.addDataSource(repast::createSVDataSource("OutDrinker_RoleCount2", outDrinker_RoleCount2, std::plus<int>()));

	StatDataSource<int>* outDrinker_RoleCount3 = mpCollector->getDataSource<int>("OutDrinker_RoleCount3");
	builder.addDataSource(repast::createSVDataSource("OutDrinker_RoleCount3", outDrinker_RoleCount3, std::plus<int>()));


	StatDataSource<double>* outQuant_RoleCount0 = mpCollector->getDataSource<double>("OutQuant_RoleCount0");
	builder.addDataSource(repast::createSVDataSource("OutQuant_RoleCount0", outQuant_RoleCount0, std::plus<double>()));

	StatDataSource<double>* outQuant_RoleCount1 = mpCollector->getDataSource<double>("OutQuant_RoleCount1");
	builder.addDataSource(repast::createSVDataSource("OutQuant_RoleCount1", outQuant_RoleCount1, std::plus<double>()));

	StatDataSource<double>* outQuant_RoleCount2 = mpCollector->getDataSource<double>("OutQuant_RoleCount2");
	builder.addDataSource(repast::createSVDataSource("OutQuant_RoleCount2", outQuant_RoleCount2, std::plus<double>()));

	StatDataSource<double>* outQuant_RoleCount3 = mpCollector->getDataSource<double>("OutQuant_RoleCount3");
	builder.addDataSource(repast::createSVDataSource("OutQuant_RoleCount3", outQuant_RoleCount3, std::plus<double>()));


	StatDataSource<int>* outFreq_RoleCount0 = mpCollector->getDataSource<int>("OutFreq_RoleCount0");
	builder.addDataSource(repast::createSVDataSource("OutFreq_RoleCount0", outFreq_RoleCount0, std::plus<int>()));

	StatDataSource<int>* outFreq_RoleCount1 = mpCollector->getDataSource<int>("OutFreq_RoleCount1");
	builder.addDataSource(repast::createSVDataSource("OutFreq_RoleCount1", outFreq_RoleCount1, std::plus<int>()));

	StatDataSource<int>* outFreq_RoleCount2 = mpCollector->getDataSource<int>("OutFreq_RoleCount2");
	builder.addDataSource(repast::createSVDataSource("OutFreq_RoleCount2", outFreq_RoleCount2, std::plus<int>()));

	StatDataSource<int>* outFreq_RoleCount3 = mpCollector->getDataSource<int>("OutFreq_RoleCount3");
	builder.addDataSource(repast::createSVDataSource("OutFreq_RoleCount3", outFreq_RoleCount3, std::plus<int>()));


	StatDataSource<int>* outHeavy_RoleCount0 = mpCollector->getDataSource<int>("OutHeavy_RoleCount0");
	builder.addDataSource(repast::createSVDataSource("OutHeavy_RoleCount0", outHeavy_RoleCount0, std::plus<int>()));

	StatDataSource<int>* outHeavy_RoleCount1 = mpCollector->getDataSource<int>("OutHeavy_RoleCount1");
	builder.addDataSource(repast::createSVDataSource("OutHeavy_RoleCount1", outHeavy_RoleCount1, std::plus<int>()));

	StatDataSource<int>* outHeavy_RoleCount2 = mpCollector->getDataSource<int>("OutHeavy_RoleCount2");
	builder.addDataSource(repast::createSVDataSource("OutHeavy_RoleCount2", outHeavy_RoleCount2, std::plus<int>()));

	StatDataSource<int>* outHeavy_RoleCount3 = mpCollector->getDataSource<int>("OutHeavy_RoleCount3");
	builder.addDataSource(repast::createSVDataSource("OutHeavy_RoleCount3", outHeavy_RoleCount3, std::plus<int>()));

	/*
	// Role stats by having/not having one of 3 roles
	StatDataSource<int>* outPop_N_M = mpCollector->getDataSource<int>("OutPop_N_M");
	builder.addDataSource(repast::createSVDataSource("OutPop_N_M", outPop_N_M, std::plus<int>()));

	StatDataSource<int>* outPop_M = mpCollector->getDataSource<int>("OutPop_M");
	builder.addDataSource(repast::createSVDataSource("OutPop_M", outPop_M, std::plus<int>()));

	StatDataSource<int>* outPop_N_E = mpCollector->getDataSource<int>("OutPop_N_E");
	builder.addDataSource(repast::createSVDataSource("OutPop_N_E", outPop_N_E, std::plus<int>()));

	StatDataSource<int>* outPop_E = mpCollector->getDataSource<int>("OutPop_E");
	builder.addDataSource(repast::createSVDataSource("OutPop_E", outPop_E, std::plus<int>()));

	StatDataSource<int>* outPop_N_P = mpCollector->getDataSource<int>("OutPop_N_P");
	builder.addDataSource(repast::createSVDataSource("OutPop_N_P", outPop_N_P, std::plus<int>()));

	StatDataSource<int>* outPop_P = mpCollector->getDataSource<int>("OutPop_P");
	builder.addDataSource(repast::createSVDataSource("OutPop_P", outPop_P, std::plus<int>()));


	StatDataSource<int>* outDrinker_N_M = mpCollector->getDataSource<int>("OutDrinker_N_M");
	builder.addDataSource(repast::createSVDataSource("OutDrinker_N_M", outDrinker_N_M, std::plus<int>()));

	StatDataSource<int>* outDrinker_M = mpCollector->getDataSource<int>("OutDrinker_M");
	builder.addDataSource(repast::createSVDataSource("OutDrinker_M", outDrinker_M, std::plus<int>()));

	StatDataSource<int>* outDrinker_N_E = mpCollector->getDataSource<int>("OutDrinker_N_E");
	builder.addDataSource(repast::createSVDataSource("OutDrinker_N_E", outDrinker_N_E, std::plus<int>()));

	StatDataSource<int>* outDrinker_E = mpCollector->getDataSource<int>("OutDrinker_E");
	builder.addDataSource(repast::createSVDataSource("OutDrinker_E", outDrinker_E, std::plus<int>()));

	StatDataSource<int>* outDrinker_N_P = mpCollector->getDataSource<int>("OutDrinker_N_P");
	builder.addDataSource(repast::createSVDataSource("OutDrinker_N_P", outDrinker_N_P, std::plus<int>()));

	StatDataSource<int>* outDrinker_P = mpCollector->getDataSource<int>("OutDrinker_P");
	builder.addDataSource(repast::createSVDataSource("OutDrinker_P", outDrinker_P, std::plus<int>()));


	StatDataSource<double>* outQuant_N_M = mpCollector->getDataSource<double>("OutQuant_N_M");
	builder.addDataSource(repast::createSVDataSource("OutQuant_N_M", outQuant_N_M, std::plus<double>()));

	StatDataSource<double>* outQuant_M = mpCollector->getDataSource<double>("OutQuant_M");
	builder.addDataSource(repast::createSVDataSource("OutQuant_M", outQuant_M, std::plus<double>()));

	StatDataSource<double>* outQuant_N_E = mpCollector->getDataSource<double>("OutQuant_N_E");
	builder.addDataSource(repast::createSVDataSource("OutQuant_N_E", outQuant_N_E, std::plus<double>()));

	StatDataSource<double>* outQuant_E = mpCollector->getDataSource<double>("OutQuant_E");
	builder.addDataSource(repast::createSVDataSource("OutQuant_E", outQuant_E, std::plus<double>()));

	StatDataSource<double>* outQuant_N_P = mpCollector->getDataSource<double>("OutQuant_N_P");
	builder.addDataSource(repast::createSVDataSource("OutQuant_N_P", outQuant_N_P, std::plus<double>()));

	StatDataSource<double>* outQuant_P = mpCollector->getDataSource<double>("OutQuant_P");
	builder.addDataSource(repast::createSVDataSource("OutQuant_P", outQuant_P, std::plus<double>()));


	StatDataSource<int>* outFreq_N_M = mpCollector->getDataSource<int>("OutFreq_N_M");
	builder.addDataSource(repast::createSVDataSource("OutFreq_N_M", outFreq_N_M, std::plus<int>()));

	StatDataSource<int>* outFreq_M = mpCollector->getDataSource<int>("OutFreq_M");
	builder.addDataSource(repast::createSVDataSource("OutFreq_M", outFreq_M, std::plus<int>()));

	StatDataSource<int>* outFreq_N_E = mpCollector->getDataSource<int>("OutFreq_N_E");
	builder.addDataSource(repast::createSVDataSource("OutFreq_N_E", outFreq_N_E, std::plus<int>()));

	StatDataSource<int>* outFreq_E = mpCollector->getDataSource<int>("OutFreq_E");
	builder.addDataSource(repast::createSVDataSource("OutFreq_E", outFreq_E, std::plus<int>()));

	StatDataSource<int>* outFreq_N_P = mpCollector->getDataSource<int>("OutFreq_N_P");
	builder.addDataSource(repast::createSVDataSource("OutFreq_N_P", outFreq_N_P, std::plus<int>()));

	StatDataSource<int>* outFreq_P = mpCollector->getDataSource<int>("OutFreq_P");
	builder.addDataSource(repast::createSVDataSource("OutFreq_P", outFreq_P, std::plus<int>()));
	*/

	// Role stats by 8 role combos
	StatDataSource<int>* outPopRole000 = mpCollector->getDataSource<int>("OutPopRole000");
	builder.addDataSource(repast::createSVDataSource("OutPopRole000", outPopRole000, std::plus<int>()));

	StatDataSource<int>* outPopRole001 = mpCollector->getDataSource<int>("OutPopRole001");
	builder.addDataSource(repast::createSVDataSource("OutPopRole001", outPopRole001, std::plus<int>()));

	StatDataSource<int>* outPopRole010 = mpCollector->getDataSource<int>("OutPopRole010");
	builder.addDataSource(repast::createSVDataSource("OutPopRole010", outPopRole010, std::plus<int>()));

	StatDataSource<int>* outPopRole011 = mpCollector->getDataSource<int>("OutPopRole011");
	builder.addDataSource(repast::createSVDataSource("OutPopRole011", outPopRole011, std::plus<int>()));

	StatDataSource<int>* outPopRole100 = mpCollector->getDataSource<int>("OutPopRole100");
	builder.addDataSource(repast::createSVDataSource("OutPopRole100", outPopRole100, std::plus<int>()));

	StatDataSource<int>* outPopRole101 = mpCollector->getDataSource<int>("OutPopRole101");
	builder.addDataSource(repast::createSVDataSource("OutPopRole101", outPopRole101, std::plus<int>()));

	StatDataSource<int>* outPopRole110 = mpCollector->getDataSource<int>("OutPopRole110");
	builder.addDataSource(repast::createSVDataSource("OutPopRole110", outPopRole110, std::plus<int>()));

	StatDataSource<int>* outPopRole111 = mpCollector->getDataSource<int>("OutPopRole111");
	builder.addDataSource(repast::createSVDataSource("OutPopRole111", outPopRole111, std::plus<int>()));


	StatDataSource<int>* outDrinkerRole000 = mpCollector->getDataSource<int>("OutDrinkerRole000");
	builder.addDataSource(repast::createSVDataSource("OutDrinkerRole000", outDrinkerRole000, std::plus<int>()));

	StatDataSource<int>* outDrinkerRole001 = mpCollector->getDataSource<int>("OutDrinkerRole001");
	builder.addDataSource(repast::createSVDataSource("OutDrinkerRole001", outDrinkerRole001, std::plus<int>()));

	StatDataSource<int>* outDrinkerRole010 = mpCollector->getDataSource<int>("OutDrinkerRole010");
	builder.addDataSource(repast::createSVDataSource("OutDrinkerRole010", outDrinkerRole010, std::plus<int>()));

	StatDataSource<int>* outDrinkerRole011 = mpCollector->getDataSource<int>("OutDrinkerRole011");
	builder.addDataSource(repast::createSVDataSource("OutDrinkerRole011", outDrinkerRole011, std::plus<int>()));

	StatDataSource<int>* outDrinkerRole100 = mpCollector->getDataSource<int>("OutDrinkerRole100");
	builder.addDataSource(repast::createSVDataSource("OutDrinkerRole100", outDrinkerRole100, std::plus<int>()));

	StatDataSource<int>* outDrinkerRole101 = mpCollector->getDataSource<int>("OutDrinkerRole101");
	builder.addDataSource(repast::createSVDataSource("OutDrinkerRole101", outDrinkerRole101, std::plus<int>()));

	StatDataSource<int>* outDrinkerRole110 = mpCollector->getDataSource<int>("OutDrinkerRole110");
	builder.addDataSource(repast::createSVDataSource("OutDrinkerRole110", outDrinkerRole110, std::plus<int>()));

	StatDataSource<int>* outDrinkerRole111 = mpCollector->getDataSource<int>("OutDrinkerRole111");
	builder.addDataSource(repast::createSVDataSource("OutDrinkerRole111", outDrinkerRole111, std::plus<int>()));

	/*
	StatDataSource<double>* outQuantRole000 = mpCollector->getDataSource<double>("OutQuantRole000");
	builder.addDataSource(repast::createSVDataSource("OutQuantRole000", outQuantRole000, std::plus<double>()));

	StatDataSource<double>* outQuantRole001 = mpCollector->getDataSource<double>("OutQuantRole001");
	builder.addDataSource(repast::createSVDataSource("OutQuantRole001", outQuantRole001, std::plus<double>()));

	StatDataSource<double>* outQuantRole010 = mpCollector->getDataSource<double>("OutQuantRole010");
	builder.addDataSource(repast::createSVDataSource("OutQuantRole010", outQuantRole010, std::plus<double>()));

	StatDataSource<double>* outQuantRole011 = mpCollector->getDataSource<double>("OutQuantRole011");
	builder.addDataSource(repast::createSVDataSource("OutQuantRole011", outQuantRole011, std::plus<double>()));

	StatDataSource<double>* outQuantRole100 = mpCollector->getDataSource<double>("OutQuantRole100");
	builder.addDataSource(repast::createSVDataSource("OutQuantRole100", outQuantRole100, std::plus<double>()));

	StatDataSource<double>* outQuantRole101 = mpCollector->getDataSource<double>("OutQuantRole101");
	builder.addDataSource(repast::createSVDataSource("OutQuantRole101", outQuantRole101, std::plus<double>()));

	StatDataSource<double>* outQuantRole110 = mpCollector->getDataSource<double>("OutQuantRole110");
	builder.addDataSource(repast::createSVDataSource("OutQuantRole110", outQuantRole110, std::plus<double>()));

	StatDataSource<double>* outQuantRole111 = mpCollector->getDataSource<double>("OutQuantRole111");
	builder.addDataSource(repast::createSVDataSource("OutQuantRole111", outQuantRole111, std::plus<double>()));


	StatDataSource<int>* outFreqRole000 = mpCollector->getDataSource<int>("OutFreqRole000");
	builder.addDataSource(repast::createSVDataSource("OutFreqRole000", outFreqRole000, std::plus<int>()));

	StatDataSource<int>* outFreqRole001 = mpCollector->getDataSource<int>("OutFreqRole001");
	builder.addDataSource(repast::createSVDataSource("OutFreqRole001", outFreqRole001, std::plus<int>()));

	StatDataSource<int>* outFreqRole010 = mpCollector->getDataSource<int>("OutFreqRole010");
	builder.addDataSource(repast::createSVDataSource("OutFreqRole010", outFreqRole010, std::plus<int>()));

	StatDataSource<int>* outFreqRole011 = mpCollector->getDataSource<int>("OutFreqRole011");
	builder.addDataSource(repast::createSVDataSource("OutFreqRole011", outFreqRole011, std::plus<int>()));

	StatDataSource<int>* outFreqRole100 = mpCollector->getDataSource<int>("OutFreqRole100");
	builder.addDataSource(repast::createSVDataSource("OutFreqRole100", outFreqRole100, std::plus<int>()));

	StatDataSource<int>* outFreqRole101 = mpCollector->getDataSource<int>("OutFreqRole101");
	builder.addDataSource(repast::createSVDataSource("OutFreqRole101", outFreqRole101, std::plus<int>()));

	StatDataSource<int>* outFreqRole110 = mpCollector->getDataSource<int>("OutFreqRole110");
	builder.addDataSource(repast::createSVDataSource("OutFreqRole110", outFreqRole110, std::plus<int>()));

	StatDataSource<int>* outFreqRole111 = mpCollector->getDataSource<int>("OutFreqRole111");
	builder.addDataSource(repast::createSVDataSource("OutFreqRole111", outFreqRole111, std::plus<int>()));


	StatDataSource<int>* outHeavyRole000 = mpCollector->getDataSource<int>("OutHeavyRole000");
	builder.addDataSource(repast::createSVDataSource("OutHeavyRole000", outHeavyRole000, std::plus<int>()));

	StatDataSource<int>* outHeavyRole001 = mpCollector->getDataSource<int>("OutHeavyRole001");
	builder.addDataSource(repast::createSVDataSource("OutHeavyRole001", outHeavyRole001, std::plus<int>()));

	StatDataSource<int>* outHeavyRole010 = mpCollector->getDataSource<int>("OutHeavyRole010");
	builder.addDataSource(repast::createSVDataSource("OutHeavyRole010", outHeavyRole010, std::plus<int>()));

	StatDataSource<int>* outHeavyRole011 = mpCollector->getDataSource<int>("OutHeavyRole011");
	builder.addDataSource(repast::createSVDataSource("OutHeavyRole011", outHeavyRole011, std::plus<int>()));

	StatDataSource<int>* outHeavyRole100 = mpCollector->getDataSource<int>("OutHeavyRole100");
	builder.addDataSource(repast::createSVDataSource("OutHeavyRole100", outHeavyRole100, std::plus<int>()));

	StatDataSource<int>* outHeavyRole101 = mpCollector->getDataSource<int>("OutHeavyRole101");
	builder.addDataSource(repast::createSVDataSource("OutHeavyRole101", outHeavyRole101, std::plus<int>()));

	StatDataSource<int>* outHeavyRole110 = mpCollector->getDataSource<int>("OutHeavyRole110");
	builder.addDataSource(repast::createSVDataSource("OutHeavyRole110", outHeavyRole110, std::plus<int>()));

	StatDataSource<int>* outHeavyRole111 = mpCollector->getDataSource<int>("OutHeavyRole111");
	builder.addDataSource(repast::createSVDataSource("OutHeavyRole111", outHeavyRole111, std::plus<int>()));
	*/
}

//read parameters from file and store in a table
void RolesModel::readRankFileForTheory(std::string rankFileName) {
#ifdef DEBUG
		std::cout << "Reading the file: " << rankFileName <<std::endl;
#endif
	ifstream myfile(rankFileName);
	if (myfile.is_open()) {
		//read the csv file
		infoTable = readCSV(myfile);
		myfile.close();
	} else {
		std::cerr << "Unable to open file: " << rankFileName << std::endl;
	}

	//find index of theory-specific variables
	std::vector<string> headerLine = infoTable.front();
	for (int i = 0; i < headerLine.size(); ++i) {
		if (headerLine[i]=="roles.marital.involvement.level") mIndexMaritalInvolvement = i;
		if (headerLine[i]=="roles.parenthood.involvement.level") mIndexParenthoodInvolvement = i;
		if (headerLine[i]=="roles.employment.involvement.level") mIndexEmploymentInvolvement = i;
		if (headerLine[i]=="roles.ability.of.coping.role.strain") mIndexAbilityCopingRoleStrain = i;
		if (headerLine[i]=="roles.skill") mIndexRoleSkill = i;
		//if (headerLine[i]=="role.disposition.baseline.mean") mIndexDispositionBaselineMean = i;
		//if (headerLine[i]=="role.disposition.baseline.sd") mIndexDispositionBaselineSD = i;
		if (headerLine[i]=="roles.gateway.disposition.initialization") mIndexGatewayDispositionInitialization = i;
	}
}

void RolesModel::initMediatorAndTheoryWithRandomParameters(Agent *agent) {
	//create theory(ies) and a mediator for each agent
	std::vector<Theory*> theoryList;
	theoryList.push_back( new RolesTheory(&context, (RolesEntity *) structuralEntityList[0]) );
	TheoryMediator *mediator = new MediatorForOneTheory(theoryList);

	//link agent with the mediator
	agent->setMediator(mediator);
}

void RolesModel::initMediatorAndTheoryFromFile(Agent *agent, std::vector<std::string> info) {
	//get agent id, read theory-specific parameters from file
	repast::AgentId agentId = agent->getId();
	int id = agentId.id();
	//obtaining corresponding value for agent
	//int educationStatus = repast::strToInt(educationStatusVector[id]);//not considering in Iteration 1
	//int levelofInvolvementEducation = repast::strToInt(levelofInvolvementEducationVector[id]);//not considering in Iteration 1

	double levelofInvolvementMarriage = repast::strToDouble(info[mIndexMaritalInvolvement]);
	double levelofInvolvementParenthood = repast::strToDouble(info[mIndexParenthoodInvolvement]);
	double levelofInvolvementEmployment = repast::strToDouble(info[mIndexEmploymentInvolvement]);
	double abilityofCopingRoleStrain = repast::strToDouble(info[mIndexAbilityCopingRoleStrain]);
	double roleSkill = repast::strToDouble(info[mIndexRoleSkill]);
	double gatewayDispositionInitialization = repast::strToDouble(info[mIndexGatewayDispositionInitialization]);
	//int expRoleOverload = repast::strToInt(expRoleOverloadVector[0]);
	//role load
	double roleLoadBeta1 = repast::strToDouble(roleLoadBeta1Vector[0]);//iteration2
	double roleLoadBeta2 = repast::strToDouble(roleLoadBeta2Vector[0]);//iteration2
	double roleLoadBeta3 = repast::strToDouble(roleLoadBeta3Vector[0]);//iteration2
	double roleLoadBeta4 = repast::strToDouble(roleLoadBeta4Vector[0]);//iteration2
	//role drinking opportunity
	double roleDrinkingOpportunityBeta1 = repast::strToDouble(roleDrinkingOpportunityBeta1Vector[0]);//iteration2
	double roleDrinkingOpportunityBeta2 = repast::strToDouble(roleDrinkingOpportunityBeta2Vector[0]);//iteration2
	double roleDrinkingOpportunityBeta3 = repast::strToDouble(roleDrinkingOpportunityBeta3Vector[0]);//iteration2
	double roleDrinkingOpportunityBeta3b = repast::strToDouble(roleDrinkingOpportunityBeta3bVector[0]);//iteration2
	double roleDrinkingOpportunityBeta4 = repast::strToDouble(roleDrinkingOpportunityBeta4Vector[0]);//iteration2

	//role gateway disposition
	double roleStrainAffectingGatewayDispositionBeta = repast::strToDouble(roleStrainAffectingGatewayDispositionBetaVector[0]);//iteration2
	double roleStrainAffectingNextDrinkDispositionBeta = repast::strToDouble(roleStrainAffectingNextDrinkDispositionBetaVector[0]);//iteration2

	//double rolesDispositionBaselineMean = repast::strToDouble(infoTable[id][mIndexDispositionBaselineMean]);
	//double rolesDispositionBaselineSD = repast::strToDouble(infoTable[id][mIndexDispositionBaselineSD]);
	double rolesSocialisationBetaMoreRoles = repast::strToDouble(roleSocialisationBetaMoreRolesVector[0]);
	double rolesSocialisationBetaLessRoles = repast::strToDouble(roleSocialisationBetaLessRolesVector[0]);
	//double rolesDispositionBetaAffectedByRoleStrainMale = repast::strToDouble(rolesDispositionBetaAffectedByRoleStrainMaleVector[0]);
	//double rolesDispositionBetaAffectedByRoleStrainFemale = repast::strToDouble(rolesDispositionBetaAffectedByRoleStrainFemaleVector[0]);
	double baselineOpportunity = repast::strToDouble(baselineOpportunityVector[0]);
	//int rolesDiscontinuity = repast::strToInt(rolesDiscontinuityVector[id]);//not considering in Iteration 1
	//int roleStrain = repast::strToInt(roleStrainVector[id]);

	//create theory(ies) and a mediator for each agent
	std::vector<Theory*> theoryList(1); //vector of 1 theory
	theoryList[0] = new RolesTheory(&context, \
									(RolesEntity *) structuralEntityList[0], \
									//educationStatus, //not considering in Iteration 1
									//levelofInvolvementEducation, //not considering in Iteration 1
									levelofInvolvementMarriage, \
									levelofInvolvementParenthood, \
									levelofInvolvementEmployment, \
									abilityofCopingRoleStrain, \
									//expRoleOverload,
									roleSkill, \
									gatewayDispositionInitialization, \
									//rolesDispositionBaselineMean,
									//rolesDispositionBaselineSD,
									roleLoadBeta1, \
									roleLoadBeta2, \
									roleLoadBeta3, \
									roleLoadBeta4, \
									roleDrinkingOpportunityBeta1, \
									roleDrinkingOpportunityBeta2, \
									roleDrinkingOpportunityBeta3, \
									roleDrinkingOpportunityBeta3b, \
									roleDrinkingOpportunityBeta4, \
									roleStrainAffectingGatewayDispositionBeta, \
									roleStrainAffectingNextDrinkDispositionBeta, \
									rolesSocialisationBetaMoreRoles, \
									rolesSocialisationBetaLessRoles, \
									//rolesDispositionBetaAffectedByRoleStrainMale,
									//rolesDispositionBetaAffectedByRoleStrainFemale,
									baselineOpportunity, \
									mBalanceOpportunity, \
									mSocialisationSpeed
									//rolesDiscontinuity, //not considering in Iteration 1
									); //only a RolesTheory in the list
	TheoryMediator *mediator = new MediatorForOneTheory(theoryList);

	//link agent with the mediator
	agent->setMediator(mediator);
}

void RolesModel::initStatisticCollector() {
	mpCollector = new RolesStatisticsCollector(&context);
}

void RolesModel::initForTheory(repast::ScheduleRunner& runner) {
	RolesEntity *pRoleEntity = (RolesEntity *) structuralEntityList[0];
	pRoleEntity->doTransformation();
}
