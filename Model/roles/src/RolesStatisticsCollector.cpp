#include "RolesStatisticsCollector.h"

RolesStatisticsCollector::RolesStatisticsCollector(repast::SharedContext<Agent>* pPopulation) : StatisticsCollector(pPopulation) {}

void RolesStatisticsCollector::collectTheoryAgentStatistics() {
	int vPop_RoleCount0=0;
	int vPop_RoleCount1=0;
	int vPop_RoleCount2=0;
	int vPop_RoleCount3=0;

	int vDrinker_RoleCount0=0;
	int vDrinker_RoleCount1=0;
	int vDrinker_RoleCount2=0;
	int vDrinker_RoleCount3=0;

	double vQuant_RoleCount0=0;
	double vQuant_RoleCount1=0;
	double vQuant_RoleCount2=0;
	double vQuant_RoleCount3=0;

	int vFreq_RoleCount0=0;
	int vFreq_RoleCount1=0;
	int vFreq_RoleCount2=0;
	int vFreq_RoleCount3=0;

	int vHeavy_RoleCount0=0;
	int vHeavy_RoleCount1=0;
	int vHeavy_RoleCount2=0;
	int vHeavy_RoleCount3=0;

	//// for role prev ////
	int vPopRole000 = 0;
	int vPopRole001 = 0;
	int vPopRole010 = 0;
	int vPopRole011 = 0;
	int vPopRole100 = 0;
	int vPopRole101 = 0;
	int vPopRole110 = 0;
	int vPopRole111 = 0;

	int vDrinkerRole000 = 0;
	int vDrinkerRole001 = 0;
	int vDrinkerRole010 = 0;
	int vDrinkerRole011 = 0;
	int vDrinkerRole100 = 0;
	int vDrinkerRole101 = 0;
	int vDrinkerRole110 = 0;
	int vDrinkerRole111 = 0;
	//// for role prev ////

	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getAge() >= 18) {
			//// 4 role counts: no role, 1, 2, 3 roles /////
			if ((*iter)->getMaritalStatus() +
					(*iter)->getParenthoodStatus() +
					(*iter)->getEmploymentStatus() == 0) {
				vPop_RoleCount0 += 1;
				if ((*iter)->isHaveKDrinksOverNDays(365, 1)) {
					vDrinker_RoleCount0 += 1;
					vQuant_RoleCount0 += (*iter)->getAvgDrinksNDays(30);
					vFreq_RoleCount0 += (*iter)->getNumDaysHavingKDrinksOverNDays(30,1);
					vHeavy_RoleCount0 += (*iter)->getNumDaysHavingKDrinksOverNDays(30,5);
				}
			} else if ((*iter)->getMaritalStatus() +
					(*iter)->getParenthoodStatus() +
					(*iter)->getEmploymentStatus() == 1) {
				vPop_RoleCount1 += 1;
				if ((*iter)->isHaveKDrinksOverNDays(365, 1)) {
					vDrinker_RoleCount1 += 1;
					vQuant_RoleCount1 += (*iter)->getAvgDrinksNDays(30);
					vFreq_RoleCount1 += (*iter)->getNumDaysHavingKDrinksOverNDays(30,1);
					vHeavy_RoleCount1 += (*iter)->getNumDaysHavingKDrinksOverNDays(30,5);
				}
			} else if ((*iter)->getMaritalStatus() +
					(*iter)->getParenthoodStatus() +
					(*iter)->getEmploymentStatus() == 2) {
				vPop_RoleCount2 += 1;
				if ((*iter)->isHaveKDrinksOverNDays(365, 1)) {
					vDrinker_RoleCount2 += 1;
					vQuant_RoleCount2 += (*iter)->getAvgDrinksNDays(30);
					vFreq_RoleCount2 += (*iter)->getNumDaysHavingKDrinksOverNDays(30,1);
					vHeavy_RoleCount2 += (*iter)->getNumDaysHavingKDrinksOverNDays(30,5);
				}
			} else if ((*iter)->getMaritalStatus() +
					(*iter)->getParenthoodStatus() +
					(*iter)->getEmploymentStatus() == 3) {
				vPop_RoleCount3 += 1;
				if ((*iter)->isHaveKDrinksOverNDays(365, 1)) {
					vDrinker_RoleCount3 += 1;
					vQuant_RoleCount3 += (*iter)->getAvgDrinksNDays(30);
					vFreq_RoleCount3 += (*iter)->getNumDaysHavingKDrinksOverNDays(30,1);
					vHeavy_RoleCount3 += (*iter)->getNumDaysHavingKDrinksOverNDays(30,5);
				}
			}

			//// for role prevalence ////
			if ((*iter)->getMaritalStatus() == 0 &&
					(*iter)->getParenthoodStatus() == 0 &&
					(*iter)->getEmploymentStatus() == 0) {
				vPopRole000 += 1;
				if ((*iter)->isHaveKDrinksOverNDays(365, 1)) {
					vDrinkerRole000 += 1;
				}
			}
			if ((*iter)->getMaritalStatus() == 0 &&
					(*iter)->getParenthoodStatus() == 0 &&
					(*iter)->getEmploymentStatus() == 1) {
				vPopRole001 += 1;
				if ((*iter)->isHaveKDrinksOverNDays(365, 1)) {
					vDrinkerRole001 += 1;
				}
			}
			if ((*iter)->getMaritalStatus() == 0 &&
					(*iter)->getParenthoodStatus() == 1 &&
					(*iter)->getEmploymentStatus() == 0) {
				vPopRole010 += 1;
				if ((*iter)->isHaveKDrinksOverNDays(365, 1)) {
					vDrinkerRole010 += 1;
				}
			}
			if ((*iter)->getMaritalStatus() == 0 &&
					(*iter)->getParenthoodStatus() == 1 &&
					(*iter)->getEmploymentStatus() == 1) {
				vPopRole011 += 1;
				if ((*iter)->isHaveKDrinksOverNDays(365, 1)) {
					vDrinkerRole011 += 1;
				}
			}
			if ((*iter)->getMaritalStatus() == 1 &&
					(*iter)->getParenthoodStatus() == 0 &&
					(*iter)->getEmploymentStatus() == 0) {
				vPopRole100 += 1;
				if ((*iter)->isHaveKDrinksOverNDays(365, 1)) {
					vDrinkerRole100 += 1;
				}
			}
			if ((*iter)->getMaritalStatus() == 1 &&
					(*iter)->getParenthoodStatus() == 0 &&
					(*iter)->getEmploymentStatus() == 1) {
				vPopRole101 += 1;
				if ((*iter)->isHaveKDrinksOverNDays(365, 1)) {
					vDrinkerRole101 += 1;
				}
			}
			if ((*iter)->getMaritalStatus() == 1 &&
					(*iter)->getParenthoodStatus() == 1 &&
					(*iter)->getEmploymentStatus() == 0) {
				vPopRole110 += 1;
				if ((*iter)->isHaveKDrinksOverNDays(365, 1)) {
					vDrinkerRole110 += 1;
				}
			}
			if ((*iter)->getMaritalStatus() == 1 &&
					(*iter)->getParenthoodStatus() == 1 &&
					(*iter)->getEmploymentStatus() == 1) {
				vPopRole111 += 1;
				if ((*iter)->isHaveKDrinksOverNDays(365, 1)) {
					vDrinkerRole111 += 1;
				}
			}
		}

		iter++;
	}

	set("OutPop_RoleCount0",vPop_RoleCount0);
	set("OutPop_RoleCount1",vPop_RoleCount1);
	set("OutPop_RoleCount2",vPop_RoleCount2);
	set("OutPop_RoleCount3",vPop_RoleCount3);

	set("OutDrinker_RoleCount0",vDrinker_RoleCount0);
	set("OutDrinker_RoleCount1",vDrinker_RoleCount1);
	set("OutDrinker_RoleCount2",vDrinker_RoleCount2);
	set("OutDrinker_RoleCount3",vDrinker_RoleCount3);

	set("OutQuant_RoleCount0",vQuant_RoleCount0);
	set("OutQuant_RoleCount1",vQuant_RoleCount1);
	set("OutQuant_RoleCount2",vQuant_RoleCount2);
	set("OutQuant_RoleCount3",vQuant_RoleCount3);

	set("OutFreq_RoleCount0",vFreq_RoleCount0);
	set("OutFreq_RoleCount1",vFreq_RoleCount1);
	set("OutFreq_RoleCount2",vFreq_RoleCount2);
	set("OutFreq_RoleCount3",vFreq_RoleCount3);

	set("OutHeavy_RoleCount0",vHeavy_RoleCount0);
	set("OutHeavy_RoleCount1",vHeavy_RoleCount1);
	set("OutHeavy_RoleCount2",vHeavy_RoleCount2);
	set("OutHeavy_RoleCount3",vHeavy_RoleCount3);


	set("OutPopRole000",vPopRole000);
	set("OutPopRole001",vPopRole001);
	set("OutPopRole010",vPopRole010);
	set("OutPopRole011",vPopRole011);
	set("OutPopRole100",vPopRole100);
	set("OutPopRole101",vPopRole101);
	set("OutPopRole110",vPopRole110);
	set("OutPopRole111",vPopRole111);

	set("OutDrinkerRole000",vDrinkerRole000);
	set("OutDrinkerRole001",vDrinkerRole001);
	set("OutDrinkerRole010",vDrinkerRole010);
	set("OutDrinkerRole011",vDrinkerRole011);
	set("OutDrinkerRole100",vDrinkerRole100);
	set("OutDrinkerRole101",vDrinkerRole101);
	set("OutDrinkerRole110",vDrinkerRole110);
	set("OutDrinkerRole111",vDrinkerRole111);


	/*
	int vPop_N_M =0;
	int vPop_M =0;
	int vPop_N_E =0;
	int vPop_E =0;
	int vPop_N_P =0;
	int vPop_P =0;

	int vDrinker_N_M =0;
	int vDrinker_M =0;
	int vDrinker_N_E =0;
	int vDrinker_E =0;
	int vDrinker_N_P =0;
	int vDrinker_P =0;

	double vQuant_N_M =0;
	double vQuant_M =0;
	double vQuant_N_E =0;
	double vQuant_E =0;
	double vQuant_N_P =0;
	double vQuant_P =0;

	int vFreq_N_M =0;
	int vFreq_M =0;
	int vFreq_N_E =0;
	int vFreq_E =0;
	int vFreq_N_P =0;
	int vFreq_P =0;


	int vPopRole000 = 0;
	int vPopRole001 = 0;
	int vPopRole010 = 0;
	int vPopRole011 = 0;
	int vPopRole100 = 0;
	int vPopRole101 = 0;
	int vPopRole110 = 0;
	int vPopRole111 = 0;

	int vDrinkerRole000 = 0;
	int vDrinkerRole001 = 0;
	int vDrinkerRole010 = 0;
	int vDrinkerRole011 = 0;
	int vDrinkerRole100 = 0;
	int vDrinkerRole101 = 0;
	int vDrinkerRole110 = 0;
	int vDrinkerRole111 = 0;

	double vQuantRole000 = 0;
	double vQuantRole001 = 0;
	double vQuantRole010 = 0;
	double vQuantRole011 = 0;
	double vQuantRole100 = 0;
	double vQuantRole101 = 0;
	double vQuantRole110 = 0;
	double vQuantRole111 = 0;

	int vFreqRole000 = 0;
	int vFreqRole001 = 0;
	int vFreqRole010 = 0;
	int vFreqRole011 = 0;
	int vFreqRole100 = 0;
	int vFreqRole101 = 0;
	int vFreqRole110 = 0;
	int vFreqRole111 = 0;

	int vHeavyRole000 = 0;
	int vHeavyRole001 = 0;
	int vHeavyRole010 = 0;
	int vHeavyRole011 = 0;
	int vHeavyRole100 = 0;
	int vHeavyRole101 = 0;
	int vHeavyRole110 = 0;
	int vHeavyRole111 = 0;


	repast::SharedContext<Agent>::const_local_iterator iter =
			mpPopulation->localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd =
			mpPopulation->localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getMaritalStatus() == 0) {
			vPop_N_M += 1;
			if ((*iter)->isHaveKDrinksOverNDays(365, 1)) {
				vDrinker_N_M += 1;
				vQuant_N_M += (*iter)->getAvgDrinksNDays(30);
				vFreq_N_M += (*iter)->getNumDaysHavingKDrinksOverNDays(30,1);
			}
		}
		if ((*iter)->getMaritalStatus() == 1) {
			vPop_M += 1;
			if ((*iter)->isHaveKDrinksOverNDays(365, 1)) {
				vDrinker_M += 1;
				vQuant_M += (*iter)->getAvgDrinksNDays(30);
				vFreq_M += (*iter)->getNumDaysHavingKDrinksOverNDays(30,1);
			}
		}
		if ((*iter)->getEmploymentStatus() == 0) {
			vPop_N_E += 1;
			if ((*iter)->isHaveKDrinksOverNDays(365, 1)) {
				vDrinker_N_E += 1;
				vQuant_N_E += (*iter)->getAvgDrinksNDays(30);
				vFreq_N_E += (*iter)->getNumDaysHavingKDrinksOverNDays(30,1);
			}
		}
		if ((*iter)->getEmploymentStatus() == 1) {
			vPop_E += 1;
			if ((*iter)->isHaveKDrinksOverNDays(365, 1)) {
				vDrinker_E += 1;
				vQuant_E += (*iter)->getAvgDrinksNDays(30);
				vFreq_E += (*iter)->getNumDaysHavingKDrinksOverNDays(30,1);
			}
		}
		if ((*iter)->getParenthoodStatus() == 0) {
			vPop_N_P += 1;
			if ((*iter)->isHaveKDrinksOverNDays(365, 1)) {
				vDrinker_N_P += 1;
				vQuant_N_P += (*iter)->getAvgDrinksNDays(30);
				vFreq_N_P += (*iter)->getNumDaysHavingKDrinksOverNDays(30,1);
			}
		}
		if ((*iter)->getParenthoodStatus() == 1) {
			vPop_P += 1;
			if ((*iter)->isHaveKDrinksOverNDays(365, 1)) {
				vDrinker_P += 1;
				vQuant_P += (*iter)->getAvgDrinksNDays(30);
				vFreq_P += (*iter)->getNumDaysHavingKDrinksOverNDays(30,1);
			}
		}


		if ((*iter)->getMaritalStatus() == 0 &&
				(*iter)->getParenthoodStatus() == 0 &&
				(*iter)->getEmploymentStatus() == 0) {
			vPopRole000 += 1;
			if ((*iter)->isHaveKDrinksOverNDays(365, 1)) {
				vDrinkerRole000 += 1;
				vQuantRole000 += (*iter)->getAvgDrinksNDays(30);
				vFreqRole000 += (*iter)->getNumDaysHavingKDrinksOverNDays(30,1);
				vHeavyRole000 += (*iter)->getNumDaysHavingKDrinksOverNDays(30,5);
			}
		}
		if ((*iter)->getMaritalStatus() == 0 &&
				(*iter)->getParenthoodStatus() == 0 &&
				(*iter)->getEmploymentStatus() == 1) {
			vPopRole001 += 1;
			if ((*iter)->isHaveKDrinksOverNDays(365, 1)) {
				vDrinkerRole001 += 1;
				vQuantRole001 += (*iter)->getAvgDrinksNDays(30);
				vFreqRole001 += (*iter)->getNumDaysHavingKDrinksOverNDays(30,1);
				vHeavyRole001 += (*iter)->getNumDaysHavingKDrinksOverNDays(30,5);
			}
		}
		if ((*iter)->getMaritalStatus() == 0 &&
				(*iter)->getParenthoodStatus() == 1 &&
				(*iter)->getEmploymentStatus() == 0) {
			vPopRole010 += 1;
			if ((*iter)->isHaveKDrinksOverNDays(365, 1)) {
				vDrinkerRole010 += 1;
				vQuantRole010 += (*iter)->getAvgDrinksNDays(30);
				vFreqRole010 += (*iter)->getNumDaysHavingKDrinksOverNDays(30,1);
				vHeavyRole010 += (*iter)->getNumDaysHavingKDrinksOverNDays(30,5);
			}
		}
		if ((*iter)->getMaritalStatus() == 0 &&
				(*iter)->getParenthoodStatus() == 1 &&
				(*iter)->getEmploymentStatus() == 1) {
			vPopRole011 += 1;
			if ((*iter)->isHaveKDrinksOverNDays(365, 1)) {
				vDrinkerRole011 += 1;
				vQuantRole011 += (*iter)->getAvgDrinksNDays(30);
				vFreqRole011 += (*iter)->getNumDaysHavingKDrinksOverNDays(30,1);
				vHeavyRole011 += (*iter)->getNumDaysHavingKDrinksOverNDays(30,5);
			}
		}
		if ((*iter)->getMaritalStatus() == 1 &&
				(*iter)->getParenthoodStatus() == 0 &&
				(*iter)->getEmploymentStatus() == 0) {
			vPopRole100 += 1;
			if ((*iter)->isHaveKDrinksOverNDays(365, 1)) {
				vDrinkerRole100 += 1;
				vQuantRole100 += (*iter)->getAvgDrinksNDays(30);
				vFreqRole100 += (*iter)->getNumDaysHavingKDrinksOverNDays(30,1);
				vHeavyRole100 += (*iter)->getNumDaysHavingKDrinksOverNDays(30,5);
			}
		}
		if ((*iter)->getMaritalStatus() == 1 &&
				(*iter)->getParenthoodStatus() == 0 &&
				(*iter)->getEmploymentStatus() == 1) {
			vPopRole101 += 1;
			if ((*iter)->isHaveKDrinksOverNDays(365, 1)) {
				vDrinkerRole101 += 1;
				vQuantRole101 += (*iter)->getAvgDrinksNDays(30);
				vFreqRole101 += (*iter)->getNumDaysHavingKDrinksOverNDays(30,1);
				vHeavyRole101 += (*iter)->getNumDaysHavingKDrinksOverNDays(30,5);
			}
		}
		if ((*iter)->getMaritalStatus() == 1 &&
				(*iter)->getParenthoodStatus() == 1 &&
				(*iter)->getEmploymentStatus() == 0) {
			vPopRole110 += 1;
			if ((*iter)->isHaveKDrinksOverNDays(365, 1)) {
				vDrinkerRole110 += 1;
				vQuantRole110 += (*iter)->getAvgDrinksNDays(30);
				vFreqRole110 += (*iter)->getNumDaysHavingKDrinksOverNDays(30,1);
				vHeavyRole110 += (*iter)->getNumDaysHavingKDrinksOverNDays(30,5);
			}
		}
		if ((*iter)->getMaritalStatus() == 1 &&
				(*iter)->getParenthoodStatus() == 1 &&
				(*iter)->getEmploymentStatus() == 1) {
			vPopRole111 += 1;
			if ((*iter)->isHaveKDrinksOverNDays(365, 1)) {
				vDrinkerRole111 += 1;
				vQuantRole111 += (*iter)->getAvgDrinksNDays(30);
				vFreqRole111 += (*iter)->getNumDaysHavingKDrinksOverNDays(30,1);
				vHeavyRole111 += (*iter)->getNumDaysHavingKDrinksOverNDays(30,5);
			}
		}

		iter++;
	}

	set("OutPop_N_M",vPop_N_M);
	set("OutPop_M",vPop_M);
	set("OutPop_N_E",vPop_N_E);
	set("OutPop_E",vPop_E);
	set("OutPop_N_P",vPop_N_P);
	set("OutPop_P",vPop_P);

	set("OutDrinker_N_M",vDrinker_N_M);
	set("OutDrinker_M",vDrinker_M);
	set("OutDrinker_N_E",vDrinker_N_E);
	set("OutDrinker_E",vDrinker_E);
	set("OutDrinker_N_P",vDrinker_N_P);
	set("OutDrinker_P",vDrinker_P);

	set("OutQuant_N_M",vQuant_N_M);
	set("OutQuant_M",vQuant_M);
	set("OutQuant_N_E",vQuant_N_E);
	set("OutQuant_E",vQuant_E);
	set("OutQuant_N_P",vQuant_N_P);
	set("OutQuant_P",vQuant_P);

	set("OutFreq_N_M",vFreq_N_M);
	set("OutFreq_M",vFreq_M);
	set("OutFreq_N_E",vFreq_N_E);
	set("OutFreq_E",vFreq_E);
	set("OutFreq_N_P",vFreq_N_P);
	set("OutFreq_P",vFreq_P);


	set("OutPopRole000",vPopRole000);
	set("OutPopRole001",vPopRole001);
	set("OutPopRole010",vPopRole010);
	set("OutPopRole011",vPopRole011);
	set("OutPopRole100",vPopRole100);
	set("OutPopRole101",vPopRole101);
	set("OutPopRole110",vPopRole110);
	set("OutPopRole111",vPopRole111);

	set("OutDrinkerRole000",vDrinkerRole000);
	set("OutDrinkerRole001",vDrinkerRole001);
	set("OutDrinkerRole010",vDrinkerRole010);
	set("OutDrinkerRole011",vDrinkerRole011);
	set("OutDrinkerRole100",vDrinkerRole100);
	set("OutDrinkerRole101",vDrinkerRole101);
	set("OutDrinkerRole110",vDrinkerRole110);
	set("OutDrinkerRole111",vDrinkerRole111);

	set("OutQuantRole000",vQuantRole000);
	set("OutQuantRole001",vQuantRole001);
	set("OutQuantRole010",vQuantRole010);
	set("OutQuantRole011",vQuantRole011);
	set("OutQuantRole100",vQuantRole100);
	set("OutQuantRole101",vQuantRole101);
	set("OutQuantRole110",vQuantRole110);
	set("OutQuantRole111",vQuantRole111);

	set("OutFreqRole000",vFreqRole000);
	set("OutFreqRole001",vFreqRole001);
	set("OutFreqRole010",vFreqRole010);
	set("OutFreqRole011",vFreqRole011);
	set("OutFreqRole100",vFreqRole100);
	set("OutFreqRole101",vFreqRole101);
	set("OutFreqRole110",vFreqRole110);
	set("OutFreqRole111",vFreqRole111);

	set("OutHeavyRole000",vHeavyRole000);
	set("OutHeavyRole001",vHeavyRole001);
	set("OutHeavyRole010",vHeavyRole010);
	set("OutHeavyRole011",vHeavyRole011);
	set("OutHeavyRole100",vHeavyRole100);
	set("OutHeavyRole101",vHeavyRole101);
	set("OutHeavyRole110",vHeavyRole110);
	set("OutHeavyRole111",vHeavyRole111);
	*/
}