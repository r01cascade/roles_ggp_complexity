/* RolesTheory.h */

#include "Agent.h"
#include "Theory.h"
#include "RolesTheory.h"
#include <math.h>
#include <string>
#include "repast_hpc/Utilities.h"

#include "repast_hpc/RepastProcess.h"

//#define DEBUG

RolesTheory::RolesTheory(repast::SharedContext<Agent> *pPopulation, RolesEntity *pRolesEntity) {

	mpPopulation = pPopulation;
	mpRolesEntity = pRolesEntity;

}

RolesTheory::RolesTheory(repast::SharedContext<Agent> *pPopulation, \
                RolesEntity *pRolesEntity, \
                //int educationStatus, //not considering in Iteration 1
                //double levelofInvolvementEducation, //not considering in Iteration 1
                double levelofInvolvementMarriage, \
                double levelofInvolvementParenthood, \
                double levelofInvolvementEmployment, \
				double abilityofCopingRoleStrain, \
				//int expRoleOverload,
				double roleSkill, \
				double gatewayDispositionInitialization, \
				//double rolesDispositionBaselineMean,
				//double rolesDispositionBaselineSD,
				double roleLoadBeta1, \
				double roleLoadBeta2, \
				double roleLoadBeta3, \
				double roleLoadBeta4, \
				double roleDrinkingOpportunityBeta1, \
				double roleDrinkingOpportunityBeta2, \
				double roleDrinkingOpportunityBeta3, \
				double roleDrinkingOpportunityBeta3b, \
				double roleDrinkingOpportunityBeta4, \
				double roleStrainAffectingGatewayDispositionBeta, \
				double roleStrainAffectingNextDrinkDispositionBeta, \
				double rolesSocialisationBetaMoreRoles, \
				double rolesSocialisationBetaLessRoles, \
				//double rolesDispositionBetaAffectedByRoleStrainMale,
				//double rolesDispositionBetaAffectedByRoleStrainFemale,
				double baselineOpportunity, \
				double balanceOpportunity, \
				double socialisationSpeed
				//double rolesDiscontinuity, //not considering in Iteration 1
              	) {

	//receiving theory specific parameters from file for initialization
	//roles
	//mEducationStatus = educationStatus;//not considering in Iteration 1
	//mLevelofInvolvementEducation = levelofInvolvementEducation;//not considering in Iteration 1
    mLevelofInvolvementMarriage = levelofInvolvementMarriage;
    mLevelofInvolvementParenthood = levelofInvolvementParenthood;
    mLevelofInvolvementEmployment = levelofInvolvementEmployment;

    //roles character
	mAbilityofCopingRoleStrain = abilityofCopingRoleStrain;
	mRoleSkill = roleSkill;
	//mRolesDiscontinuity = rolesDiscontinuity;//not considering in Iteration 1

	mBaselineOpportunity = baselineOpportunity;
	mBalanceOpportunity = balanceOpportunity;
	//mAlphaInOpporutnityCalculation = alphaInOpporutnityCalculation;

	//mExpRoleOverLoad = expRoleOverload;
	//role load
	mRoleLoadBeta1 = roleLoadBeta1; //iteration2
	mRoleLoadBeta2 = roleLoadBeta2; //iteration2
	mRoleLoadBeta3 = roleLoadBeta3; //iteration2
	mRoleLoadBeta4 = roleLoadBeta4; //iteration2
	//role opportunity
	mRoleDrinkingOpportunityBeta1 = roleDrinkingOpportunityBeta1; //iteration2
	mRoleDrinkingOpportunityBeta2 = roleDrinkingOpportunityBeta2; //iteration2
	mRoleDrinkingOpportunityBeta3 = roleDrinkingOpportunityBeta3; //iteration2
	mRoleDrinkingOpportunityBeta3b = roleDrinkingOpportunityBeta3b; //iteration2
	mRoleDrinkingOpportunityBeta4 = roleDrinkingOpportunityBeta4; //iteration2
	//role gateway disposition
	mRoleStrainAffectingGatewayDispositionBeta = roleStrainAffectingGatewayDispositionBeta;//iteration2
	mRoleStrainAffectingNextDrinkDispositionBeta = roleStrainAffectingNextDrinkDispositionBeta;//iteration2

	//mDispositionBaselineMean = rolesDispositionBaselineMean;
	//mDispositionBaselineSD = rolesDispositionBaselineSD;
	mRolesSocialisationBetaMoreRoles = rolesSocialisationBetaMoreRoles;
	mRolesSocialisationBetaLessRoles = rolesSocialisationBetaLessRoles;
	//mDispositionBetaAffectedByRoleStrainMale = rolesDispositionBetaAffectedByRoleStrainMale;
	//mDispositionBetaAffectedByRoleStrainFemale = rolesDispositionBetaAffectedByRoleStrainFemale;
	mGatewayDispositionInitialization = gatewayDispositionInitialization;

	mpPopulation = pPopulation;
	mpRolesEntity = pRolesEntity;

	//role socialisation initialising
	mBetaSocialisationSpeed = repast::Random::instance()->nextDouble() * 365;//iteration2 - this might need to be initialised from reading microsim data, but not suitable to be put it in model.props as it should be different for agents
	mSocialisationSpeed = socialisationSpeed;
}

RolesTheory::~RolesTheory(){}

void RolesTheory::doSituation() {

	//run only once for now, may extend to daily run in future
	if (!mDispositionInitialized) {
		calBaselineDisposition();
		mDispositionInitialized = true;
	}

	calRoleLoad();
	calRoleIncongruence();
	calRoleStrain();
	if (ROLES_SOCIALISATION_ON) calRoleSocialization();

	calOpportunity();

	 #ifdef DEBUG
		repast::AgentId AgentID = mpAgent->getId();
		if (AgentID.id() == IDFOROUTPUT && AgentID.currentRank() == 0){
			std::cout<<"                         "<<std::endl;
			std::cout<<"Agent ID = "<<AgentID.id()<<std::endl;
			std::cout<<"Sex = "<<mpAgent->getSex()<<std::endl;
			std::cout<<"Age = "<<mpAgent->getAge()<<std::endl;
			std::cout<<"mRoleLoad = "<<mRoleLoad<<std::endl;
			std::cout<<"mRoleIncongruence = "<< mRoleIncongruence <<std::endl;
			std::cout<<"mRoleStrain = "<< mRoleStrain <<std::endl;
			std::cout<<"MeanDrinksToday = "<<mpAgent->getMeanDrinksToday()<<std::endl;
			std::cout<<"SDDrinksToday = "<<mpAgent->getSDDrinksToday()<<std::endl;
			//std::cout<<"mOpportunity = "<<mOpportunity<<std::endl;
			std::cout<<"New Marital, Parenthood, Employment statuses = "<<mpAgent->getMaritalStatus()<<" " \
																		 <<mpAgent->getParenthoodStatus()<<" " \
																		 <<mpAgent->getEmploymentStatus()<<std::endl;
		}
	#endif
}

void RolesTheory::doGatewayDisposition() {
    /*Action mechanism determining drinking behavior*/
	double probFirstDrink;
    // calculate individual drinking value
	//first method
	probFirstDrink = (mProbOppOut + mProbOppIn) * mDispositionVector[0] * \
							(1 + mRoleStrainAffectingGatewayDispositionBeta * mTendencyToDrinkInResponseToStrain * mRoleStrain);
	
	/*if (!agentTrackingOutput.is_open()) {
		int agentID = mpAgent->getId().id();
		if ((agentID==3)) {
			std::string agentTrackingFileName("outputs/agent_" + to_string(agentID) + "_output.csv");
			agentTrackingOutput.open(agentTrackingFileName);
			agentTrackingOutput << "id, tick, ProbOppOut, ProbOppIn, mDispostionVec0, mRoleStrainAffectingGatewayDispositionBeta, mTendencyToDrinkInResponseToStrain, RoleStrain, probFirstDrink, mRoleLoad, mRoleIncongruencee" << std::endl;
		}
	}
	if ((mpAgent->getId().id()==3) && agentTrackingOutput.is_open()){
		agentTrackingOutput << mpAgent->getId().id() << "," << 
			repast::RepastProcess::instance()->getScheduleRunner().currentTick() << "," << 
			mProbOppOut << "," << 
			mProbOppIn << "," << 
			mDispositionVector[0] << "," <<
			mRoleStrainAffectingGatewayDispositionBeta << "," << 
			mTendencyToDrinkInResponseToStrain << "," << 
			mRoleStrain << "," << 
			probFirstDrink << "," << 
			mRoleLoad << "," << 
			mRoleIncongruence << std::endl;
	}*/

	//second method
	//probFirstDrink = (mProbOppOut + mProbOppIn) * mDispositionVector[0] + mRoleStrainAffectingGatewayDispositionBeta * \
	//						mTendencyToDrinkInResponseToStrain * mRoleStrain * mDispositionVector[0];

	if (repast::Random::instance()->nextDouble() < probFirstDrink) {//for verification purpose
		/* #ifdef DEBUG
			std::cout<<" mOpportunity = "<<mOpportunity<<std::endl;
		#endif */
		mpAgent->setDispositionByIndex(0, 1); //set first disposition to 1
	} else {
		mpAgent->setDispositionByIndex(0, 0); //set first disposition to 0
	}

}

void RolesTheory::doNextDrinksDisposition() {

	double modifierByRoleStrain;

	modifierByRoleStrain = (mProbOppIn + mProbOppOut) * (1 + mRoleStrainAffectingNextDrinkDispositionBeta * \
							mTendencyToDrinkInResponseToStrain * mRoleStrain);
	// fill in the vector of drinking dispositions
	for (int i=1; i<MAX_DRINKS; ++i) {
		//mDispositionVector is not tracking the role strain effect. it's only tracking the socialization effec.

		mpAgent->setDispositionByIndex(i, mDispositionVector[i] * modifierByRoleStrain);

		//std::cout<<"mDispositionVector[i] = "<<mDispositionVector[i]<<std::endl;
		//std::cout<<"mDispositionBetaAffectedByRoleStrain = "<<mDispositionBetaAffectedByRoleStrain<<std::endl;
		//std::cout<<"modifierByRoleStrain = "<<modifierByRoleStrain<<std::endl;
	}
}

// method to identify age group of the agent ranging from 0 to NUM_AGE_GROUPS -1
int RolesTheory::findAgeGroup() {
	int myAgeGroup = 0;

	if (mpAgent->getAge() <= MAX_AGE) {
		while (mpAgent->getAge() > AGE_GROUPS[myAgeGroup]) {
			myAgeGroup++;
		}
	} else {
		myAgeGroup = NUM_AGE_GROUPS - 1;
	}

	return myAgeGroup; // Age group 0 is the youngest age groups
}

void RolesTheory::doNonDrinkingActions(){

}

void RolesTheory::calRoleLoad(){
	int maritalStatus = mpAgent->getMaritalStatus();
	int parenthoodStatus = mpAgent->getParenthoodStatus();
	int employmentStatus = mpAgent->getEmploymentStatus();

	mRoleLoad = mRoleLoadBeta1 * maritalStatus * mLevelofInvolvementMarriage + \
				mRoleLoadBeta2 * parenthoodStatus * mLevelofInvolvementParenthood + \
				mRoleLoadBeta3 * employmentStatus * mLevelofInvolvementEmployment + \
				mRoleLoadBeta4 * (1 - maritalStatus) * parenthoodStatus * mLevelofInvolvementParenthood;

	/*#ifdef DEBUG
		std::cout<<"mRoleLoad = "<<mRoleLoad<<std::endl;
	#endif*/
}

void RolesTheory::calRoleIncongruence(){

	int sex = mpAgent->getSex();
	int age = mpAgent->getAge();

	double sum = 0.0;

	int maritalStatus = mpAgent->getMaritalStatus();
	int parenthoodStatus = mpAgent->getParenthoodStatus();
	int employmentStatus = mpAgent->getEmploymentStatus();

	if (age <= MAX_AGE) {
		sum += fabs(static_cast<double>(maritalStatus) - mpRolesEntity->getRoleExpectancyMarriage(sex,findAgeGroup()));
		sum += fabs(static_cast<double>(parenthoodStatus) - mpRolesEntity->getRoleExpectancyParenthood(sex,findAgeGroup()));
		sum += fabs(static_cast<double>(employmentStatus) - mpRolesEntity->getRoleExpectancyEmployment(sex,findAgeGroup()));
	}

	mRoleIncongruence = sum/MAX_INCONGRUENCE;

	/* #ifdef DEBUG
		std::cout<<"mRoleIncongruence = "<< mRoleIncongruence <<std::endl;
	#endif */
}

void RolesTheory::calRoleStrain(){

	mRoleStrain = (1 - mRoleSkill) * (mRoleLoad + mRoleIncongruence) / 2;

	/* #ifdef DEBUG
		std::cout<<"mRoleStrain = "<< mRoleStrain <<std::endl;
	#endif */
}

void RolesTheory::calOpportunity(){

	double logOddsOppOut;
	double logOddsOppIn;

	int maritalStatus = mpAgent->getMaritalStatus();
	int parenthoodStatus = mpAgent->getParenthoodStatus();
	int employmentStatus = mpAgent->getEmploymentStatus();

	logOddsOppOut = log((1-mBalanceOpportunity)*mBaselineOpportunity/(1-mBaselineOpportunity)) * (1 - mRoleDrinkingOpportunityBeta1 * mRoleLoad + mRoleDrinkingOpportunityBeta2 * employmentStatus);
	logOddsOppIn = log(mBalanceOpportunity*mBaselineOpportunity/(1-mBaselineOpportunity)) * (1 - mRoleDrinkingOpportunityBeta3b * mRoleLoad + mRoleDrinkingOpportunityBeta4 * (maritalStatus + parenthoodStatus));

	mProbOppIn = exp(logOddsOppIn) /(exp(logOddsOppIn) + exp(logOddsOppOut) + 1);
	mProbOppOut = exp(logOddsOppOut) /(exp(logOddsOppIn) + exp(logOddsOppOut) + 1);

/* //HAO old implementation of iteration2 opportunity
	logOddsOppOut = mBaselineOpportunity * (1 - mRoleDrinkingOpportunityBeta1 * mRoleLoad + mRoleDrinkingOpportunityBeta2 * employmentStatus);
	//first choice for opportunity in
	logOddsOppIn = mBaselineOpportunity * (1 - mRoleDrinkingOpportunityBeta3b * mRoleLoad + mRoleDrinkingOpportunityBeta4 * (maritalStatus + parenthoodStatus));
	//second choice for opportunity in
	//logOddsOppIn = mBaselineOpportunity * (1 + mRoleDrinkingOpportunityBeta3 * (mRoleDrinkingOpportunityBeta1 * mRoleLoad - mRoleDrinkingOpportunityBeta2 * employmentStatus));

	mProbOppIn = exp(logOddsOppIn) /(exp(logOddsOppIn) + exp(logOddsOppOut) + exp(mBaselineOpportunity));
	mProbOppOut = exp(logOddsOppOut) /(exp(logOddsOppIn) + exp(logOddsOppOut) + exp(mBaselineOpportunity));
	//mOpportunity = mBaselineOpportunity * (1 - mAlphaInOpporutnityCalculation * mRoleLoad/mMaxRoleLoad);//opportunity to drink today
*/
}

void RolesTheory::calRoleSocialization(){

	double dayBar;
	double modifier;
	double dispositionDiff;

	if (mpAgent->getRoleChangedTransient()){
		mDaysOfSocialisation = 1;
		mStateOfRoleChange = mpAgent->getRoleChangeStatus();
		mDispositionVectorOld = mDispositionVector;
		mStartSocialising = true;
	}

	//end socialisation after a year
	if (mDaysOfSocialisation == 365 && mStartSocialising) mStartSocialising = false;

	// dayBar = (mDaysOfSocialisation - 365 / 2) / (365 / mBetaSocialisationSpeed);
	dayBar = (mDaysOfSocialisation - mSocialisationSpeed) / 365;
	modifier = exp(dayBar)/(1+exp(dayBar));

	//for more roles
	if (mStateOfRoleChange == 1 && mStartSocialising){

		for (int i=0; i<MAX_DRINKS; ++i) {

			dispositionDiff = mDispositionVectorOld[i] * (1 + mRolesSocialisationBetaMoreRoles) - mDispositionVectorOld[i];

			mDispositionVector[i] = mDispositionVectorOld[i] + dispositionDiff * modifier;
		}

		/*repast::AgentId AgentID = mpAgent->getId();
		if (AgentID.id() == 201)
			std::cout<<"mDaysOfSocialisation="<<mDaysOfSocialisation<<" mDispositionVector[1]="<<mDispositionVector[1]<<std::endl;
		*/
		mDaysOfSocialisation++;
	}
	//for less roles
	else if (mStateOfRoleChange == -1 && mStartSocialising){

		for (int i=1; i<MAX_DRINKS; ++i) {

			dispositionDiff = mDispositionVectorOld[i] * (1 + mRolesSocialisationBetaLessRoles) - mDispositionVectorOld[i];

			mDispositionVector[i] = mDispositionVectorOld[i] + dispositionDiff * modifier;

		}

		/*repast::AgentId AgentID = mpAgent->getId();
		if (AgentID.id() == 201)
			std::cout<<"mDaysOfSocialisation="<<mDaysOfSocialisation<<" mDispositionVector[1]="<<mDispositionVector[1]<<std::endl;
		*/
		mDaysOfSocialisation++;
	}
 }

void RolesTheory::calBaselineDisposition(){

	double mean = mpAgent->getMeanDrinksToday();
	double sd = mpAgent->getSDDrinksToday();

	mDispositionVector.clear();

	//initialize disposition vector with baseline value
	int freq = mpAgent->getDrinkFrequencyLevel();
	int freqLowerBound[7] = {0 ,11,19,31, 54,172,337};
	int freqUpperBound[7] = {10,18,30,53,171,336,365};
	int dayLowerBound = freqLowerBound[freq-1];
	int dayUpperBound = freqUpperBound[freq-1];
	repast::IntUniformGenerator rnd = repast::Random::instance()->createUniIntGenerator(dayLowerBound, dayUpperBound);
	int randomDrinkingDaysPerYear = rnd.next();
	double desireGateway = (double)randomDrinkingDaysPerYear/365.0;

	mDispositionVector.push_back(desireGateway);
	//mDispositionVector.push_back(mGatewayDispositionInitialization);//initialize gateway disposition with value [0,1]

	for (int i=1; i<MAX_DRINKS; ++i) {

		//double disposition = 1 - (0.5 * erfc(-(i-mean)/(sd*sqrt(2))));
		std::pair<double, double> drinkPair = generateCorrectedMeanSD(mean, sd);
		double correctedMean = drinkPair.first;
		double correctedSD = drinkPair.second;
		double disposition = 1 - (0.5 * erfc(-(i-correctedMean)/(correctedSD*sqrt(2))));

		if (!std::isnan(disposition)) mDispositionVector.push_back(disposition);
		else mDispositionVector.push_back(0);

		//mpAgent->setDispositionByIndex(i, disposition);
		//#ifdef DEBUG
		//	std::cout<<"disposition = "<<disposition<<std::endl;
		//	std::cout<<"mean = "<<mpAgent->getMeanDrinksToday()<<std::endl;
		//	std::cout<<"sd = "<<mpAgent->getSDDrinksToday()<<std::endl;
		//	std::cout<<" mDispositionVector["<<i-1<<"] = "<<mDispositionVector[i-1]<<" "<<std::endl;
		//#endif
	}
}
