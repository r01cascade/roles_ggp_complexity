# About
This is the source code for the roles model and the model discovery process (grammatical evolution) by [CASCADE project](https://www.sheffield.ac.uk/cascade) for a publication on [Complexity journal](https://www.hindawi.com/journals/complexity/).

*Publication title:* Multi-objective genetic programming can improve the explanatory capabilities of mechanism-based models of social systems

*Authours:* Tuong M. Vu, Charlotte Buckley, Hao Bai, Alexandra Nielsen, Charlotte Probst, Alan Brennan, Paul Shuper, Mark Strong, and Robin C. Purshouse

This repo is hereby licensed for use under the GNU GPL version 3.

# Folders and files structure
* The Model folder is the source code of the roles simulation model. The roles model needs to be compiled before starting the model discovery process.
* The model discovery process uses PonyGE2 which needs to be downloaded separately (instruction below). The four files (cascade\_roles.bnf, cascade\_roles.txt, cascade\_roles.py, and cascade\_nodes.py) are to be used with PonyGE2.
    * cascade\_roles.txt defines the settings of the model discovery process.
    * cascade\_roles.bnf defines the grammar.
    * cascade\_roles.py calculates model errors by comparing the simulated outputs with the real-world target data.
    * cascade\_nodes.py calculates the complexity of the structure (the number of nodes with a special case of ON node counted as 2).
* In the setup below, we will set up a GERoles folder. In GERoles folder, there will be the PonyGE2 source code, the Model folder. We will also create a Models folder to contain temporary copies of the simulation model during evaluation of the model discovery process.

# Compile and run the best-calibrated model by human modellers
* Install [RepastHPC](https://repast.github.io/repast_hpc.html).
* Update the Model/roles/env file to match with the RepastHPC installation.
* In the terminal, change directory to Model/roles then execute the command: make all
* While in Model/roles, run the roles model with: mpirun -n 1 bin/main.exe props/config.props props/model.props
* After executing, the model will generate annual\_data.csv in Model/roles/outputs folder.

# Set up and run Grammatical Evolution with PonyGE2
* Download [PonyGE2 source code](https://github.com/PonyGE/PonyGE2)
* Assuming the name of the downloaded folder is GERoles.
* Create a folder Models in the folder you just downloaded (If the source files is in GERoles/src then the new foler should be GERoles/Models).
* Move the **compiled** human model folder into GERoles/Model (The location of two folders core and roles should be: GERoles/Model/core and GERoles/Model/roles).
* Move cascade\_roles.bnf to GERoles/grammars folder.
* Move cascade\_roles.txt to GERoles/parameters folder. Make sure the path in this files match with your setup (currently /home/username/...).
* Move cascade\_roles.py and cascade\_nodes.py into GERoles/src/fitness
* Run by navigating to GERoles/src then typing this command: python3 ponyge.py --parameters cascade\_roles.txt
