from fitness.base_ff_classes.base_ff import base_ff
import hashlib # to create hash from ind string
from algorithm.parameters import params # for folder name
import os # for cd class, path exist
from distutils.dir_util import mkpath, copy_tree, remove_tree # create, copy, delete temp Model folder
import subprocess # to execute shell script

import random # DEV
from collections import OrderedDict
from datetime import datetime

class cd:
    """Context manager for changing the current working directory"""
    def __init__(self, newPath):
        self.newPath = os.path.expanduser(newPath)

    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)

class cascade_roles(base_ff):
    """
    A simulation is executed via shell script.
    Fitness is read from a text file.
    """

    def __init__(self):
        # Initialise base fitness function class.
        super().__init__()

    def replace_roles_code(self, phenotype, folder_path):
        # replace follows this order because there are overlaps in variable names
        dict = OrderedDict([
            ('MaritalStatus', '(double)mMaritalStatus'), 
            ('ParenthoodStatus', '(double)mParenthoodStatus'),
            ('EmploymentStatus', '(double)mEmploymentStatus'),
            ('InvolvedxMarital', 'mLevelofInvolvementMarriage*(double)mMaritalStatus'),
            ('InvolvedxParenthood', 'mLevelofInvolvementParenthood*(double)mParenthoodStatus'),
            ('InvolvedxEmployment', 'mLevelofInvolvementEmployment*(double)mEmploymentStatus'),
            ('DiffExpectancyMarital', 'fabs(static_cast<double>(mMaritalStatus)-mpRolesEntity->getRoleExpectancyMarriage(sex,findAgeGroup()))'),
            ('DiffExpectancyParenthood', 'fabs(static_cast<double>(mParenthoodStatus)-mpRolesEntity->getRoleExpectancyParenthood(sex,findAgeGroup()))'),
            ('DiffExpectancyEmployment', 'fabs(static_cast<double>(mEmploymentStatus)-mpRolesEntity->getRoleExpectancyEmployment(sex,findAgeGroup()))'),
            ('RoleLoad', 'mRoleLoad'),
            ('RoleIncongruence', 'mRoleIncongruence'),
            ('RoleStrain', 'mRoleStrain'),
            ('ProbOppIn', 'mProbOppIn'),
            ('ProbOppOut', 'mProbOppOut'),
            ('LifetimeDispositionI','mDispositionVector[i]'),
            ('0.333','1.0/3.0')
        ])
        str = phenotype
        for bnf_text, c_variable in dict.items():
            str = str.replace(bnf_text, c_variable)
        str = str.split(' ')
        str_do_situation = str[2]+'\n' +str[3]+'\n' +str[4]+'\n';
        str_log_odds_prob_out = str[5].replace('OutMod=','logOddsOppOut = log((1-mBalanceOpportunity)*mBaselineOpportunity/(1-mBaselineOpportunity)) * ')+'\n'
        str_log_odds_prob_in = str[6].replace('InMod=','logOddsOppIn = log(mBalanceOpportunity*mBaselineOpportunity/(1-mBaselineOpportunity)) * ')+'\n'
        str_first_drink = str[7]+'\n'
        str_next_drink = str[8]+'\n'

        # modify model.props - turn mechanisms OFF (assume that ON is the default)
        model_props_file =  folder_path + '/roles/props/model.props'
        new_file = folder_path + '/roles/src/RolesTheory.cpp'
        with open(model_props_file) as f:
            new_source = f.read()
            if 'OFF' in str[0]:
                new_source = new_source.replace('role.tp.beta1=0.12351','role.tp.beta1=0')
                new_source = new_source.replace('role.tp.beta2=3.09795','role.tp.beta2=0')
            if 'OFF' in str[1]:
                new_source = new_source.replace('role.socialisation.on=1','role.socialisation.on=0')
        with open(model_props_file, 'w') as f:
            f.write(new_source)
        
        # insert lines of codes to the simulation code
        template_file = folder_path + '/roles/src/RolesTheory.template.cpp'
        new_file = folder_path + '/roles/src/RolesTheory.cpp'
        with open(template_file) as f:
            new_source = f.read()
            new_source = new_source.replace('//GE-DO-SITUATION',str_do_situation)
            new_source = new_source.replace('//GE-FIRST-DRINK',str_first_drink)
            new_source = new_source.replace('//GE-NEXT-DRINK',str_next_drink)
            new_source = new_source.replace('//GE-LOG-ODDS-OUT',str_log_odds_prob_out)
            new_source = new_source.replace('//GE-LOG-ODDS-IN',str_log_odds_prob_in)
        with open(new_file, 'w') as f:
            f.write(new_source)


    def evaluate(self, ind, **kwargs):
        # Generate hash with md5 from phenotype
        hash_object = hashlib.md5((ind.phenotype+str(datetime.now())).encode())
        folder_path = hash_object.hexdigest()

        # Create a model folder
        folder_path = params['MODELS_FOLDER'] + '/' + folder_path
        mkpath(folder_path)

        # Copy simulation into a model folder
        copy_tree(params['SIMULATION_FOLDER'], folder_path)

        # Edit roles theory with phenotype
        
        self.replace_roles_code(ind.phenotype, folder_path)

        #fitness = random.uniform(0,1) # DEV
        
        # use cd class (for context manager) to execute the simulation
        # outside the context manager we are back wherever we started.
        with cd(folder_path):
            # Run a shell script: run RepastHPC simulation, and R script for fitness, wait
            process = subprocess.Popen('bash compileThenRun.sh', shell=True, stdout=subprocess.PIPE)
            process.wait()
            
            # Read fitness from file
            if process.returncode != 0: #if error
                fitness = base_ff.default_fitness # bad fitness
            else:
                # Read fitness from file
                f = open("fitness.out", "r")
                fitness = float(f.readline())
            '''
            print('=================')
            print(ind.phenotype.replace('; ',';\n'))
            print('=> Fitness: ', fitness)
            print('=================')
            '''
        
        # Delete a model folder
        remove_tree(folder_path)
        
        return fitness
